package screenShot;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.testng.reporters.Files;

public class ScreenShot {

	static WebDriver driver;
	
	public ScreenShot(WebDriver driver){
		this.driver=driver;
		
		
	}
	
	public static void captureScreenShot() throws IOException
	{
		// Take screenshot and store as a file format
		File src = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
	
			
			FileUtils.copyFile(src, new File("C:\\selenium\\"+System.currentTimeMillis()+".png"));
			
		
		
	}

}