package pegaselenium.apply;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.ElementNotVisibleException;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;

import pegaselenium.utils.DriverPage;
import pegaselenium.utils.Utilities;

public class ApplicationDetails extends DriverPage {

	public ApplicationDetails(WebDriver driver) {
		super(driver);
		// TODO Auto-generated constructor stub
	}

	public void selectLicence(String licType,String natTi) throws InterruptedException {
		Select classOfLicence = new Select(driver.findElement(By.id("LicenseType")));
		Thread.sleep(2000);
		classOfLicence.selectByValue(licType);
		Thread.sleep(4000);
		if(licType.contains("Standard"))
			selectNativeTitle(natTi);
	}

	public void selectNativeTitle(String natTi){
		
		int framesForReview = driver.findElements(By.tagName("iframe")).size();
		
		System.out.println("The iframes are"+framesForReview);
		//driver.switchTo().frame(0);
		
		if(natTi.contains("New")){
			driver.findElement(By.xpath("//input[@id='NativeTitlesATSN']/../label")).click();
		}
		else if(natTi.contains("NTE")){
			driver.findElement(By.xpath("//input[@id='NativeTitlesNTE']/../label")).click();
		}
		else if(natTi.contains("ENTE")){
			driver.findElement(By.xpath("//input[@id='NativeTitlesENTE']/../label")).click();
		}
		else if(natTi.contains("RTN")){
			driver.findElement(By.xpath("//input[@id='NativeTitlesRTN']/../label")).click();
		}
		else if(natTi.contains("ATN")){
			driver.findElement(By.xpath("//input[@id='NativeTitlesATN']/../label")).click();
		}
		else
			
			driver.findElement(By.xpath("//input[@id='NativeTitlesENTE']/../label")).click();
	}
	public void setEffectiveDate() {

		try {

			WebElement elem = driver.findElement(By.id("EffectiveDate"));
			if (elem.isDisplayed())
				elem.sendKeys(Utilities.getCurrentDate()); // Date;
		}

		catch (NoSuchElementException e) {
			System.out.println(e.getMessage());
		} catch (ElementNotVisibleException e) {
			System.out.println(e.getMessage());
		} catch (StaleElementReferenceException e) {
			System.out.println(e.getMessage());
		}
	}

	public void selectMineralGroup1(String grp) throws InterruptedException {

		try {
			
			if(grp.contains("1"))
				driver.findElement(By.xpath("//input[contains(@id,'pySelected1')]")).click();
			else if(grp.contains("2"))
				driver.findElement(By.xpath("//input[contains(@id,'pySelected2')]")).click();
			else if(grp.contains("3"))
				driver.findElement(By.xpath("//input[contains(@id,'pySelected3')]")).click();
			else if(grp.contains("4"))
				driver.findElement(By.xpath("//input[contains(@id,'pySelected4')]")).click();
			else if(grp.contains("5"))
				driver.findElement(By.xpath("//input[contains(@id,'pySelected5')]")).click();
			else if(grp.contains("6"))
				driver.findElement(By.xpath("//input[contains(@id,'pySelected6')]")).click();
			else if(grp.contains("7"))
				driver.findElement(By.xpath("//input[contains(@id,'pySelected7')]")).click();
			else if(grp.contains("8"))
				driver.findElement(By.xpath("//input[contains(@id,'pySelected8')]")).click();
			else if(grp.contains("10"))
				driver.findElement(By.xpath("//input[contains(@id,'pySelected9')]")).click();
			else if(grp.contains("11"))
				driver.findElement(By.xpath("//input[contains(@id,'pySelected10')]")).click();
			else 
				driver.findElement(By.xpath("//input[contains(@id,'pySelected2')]")).click();
		/*
			List<WebElement> li = driver.findElements(By.xpath("//input[@type='checkbox']"));

			for (int i = 0; i < li.size(); i++) {
				System.out.println("the number of checkboxes are " + li.size());
				if (!li.get(i).isSelected()) {
					li.get(0).click();
				}
				Thread.sleep(4000);
			}

			int framesInAssess = driver.findElements(By.tagName("iframe")).size();

			Thread.sleep(2000);

			System.out.println("The iframes in Assess are" + framesInAssess);
			if (framesInAssess == 3) {
				driver.switchTo().frame(2);
			} else if (framesInAssess == 2) {
				driver.switchTo().frame(1);
			} else {
				driver.switchTo().frame(0);
			}*/
		} catch (StaleElementReferenceException e) {
			System.out.println(e.getMessage());
		}
	}

	public void selectMineralGroup8() throws InterruptedException {

		try {

			List<WebElement> li = driver.findElements(By.xpath("//input[@type='checkbox']"));

			for (int i = 0; i < li.size(); i++) {
				System.out.println("the number of checkboxes are " + li.size());
				if (!li.get(i).isSelected()) {
					li.get(7).click();
				}
				Thread.sleep(4000);
			}

			int framesInAssess = driver.findElements(By.tagName("iframe")).size();

			Thread.sleep(2000);

			System.out.println("The iframes in Assess are" + framesInAssess);
			if (framesInAssess == 3) {
				driver.switchTo().frame(2);
			} else if (framesInAssess == 2) {
				driver.switchTo().frame(1);
			} else {
				driver.switchTo().frame(0);
			}
		} catch (StaleElementReferenceException e) {
			System.out.println(e.getMessage());
		}
	}

	public void selectMineralGroup() throws InterruptedException {

		try {

			List<WebElement> li = driver.findElements(By.xpath("//input[@type='checkbox']"));

			for (int i = 0; i < li.size(); i++) {
				System.out.println("the number of checkboxes are " + li.size());
				if (!li.get(i).isSelected()) {
					li.get(i).click();
				}
				Thread.sleep(4000);
			}
		} catch (StaleElementReferenceException e) {
			System.out.println(e.getMessage());
		}
	}

	public void selectMineralGroup11() throws InterruptedException {

		try {

			List<WebElement> li = driver.findElements(By.xpath("//input[@type='checkbox']"));

			for (int i = 0; i < li.size(); i++) {
				System.out.println("the number of checkboxes are " + li.size());
				if (!li.get(i).isSelected()) {
					li.get(9).click();
				}
				Thread.sleep(4000);
			}

			int framesInAssess = driver.findElements(By.tagName("iframe")).size();

			Thread.sleep(2000);

			System.out.println("The iframes in Assess are" + framesInAssess);
			if (framesInAssess == 3) {
				driver.switchTo().frame(2);
			} else if (framesInAssess == 2) {
				driver.switchTo().frame(1);
			} else {
				driver.switchTo().frame(0);
			}
		} catch (StaleElementReferenceException e) {
			System.out.println(e.getMessage());
		}
	}
	public void selectMineralAllocationArea(String flag) throws InterruptedException {
			
			
			try {
				
				if(flag.contains("Yes")){
					driver.findElement(By.xpath("//input[@id='MineralAllocAreaYes']/../label")).click();
					Thread.sleep(2000);
					driver.findElement(By.id("AlloctnAreaConsentDate")).sendKeys(Utilities.getCurrentDate());
				}
					
				
			} catch (StaleElementReferenceException e) {
				System.out.println(e.getMessage());
			} catch (NoSuchElementException e) {
				System.out.println(e.getMessage());
			} catch (ElementNotVisibleException e) {
				System.out.println(e.getMessage());
			}
	
		}
	public void selectMinisterConsent(String flag) throws InterruptedException {
		
		
		try {
			
			if(flag.contains("Yes")){
				driver.findElement(By.xpath("//input[@id='ConsentYes']/../label")).click();
				enterDateForMinistersConsent();
			}
				
			else
				
				driver.findElement(By.xpath("//input[@id='ConsentNo']/../label")).click();
			List<WebElement> li = driver.findElements(By.xpath("//div[@data-test-id='20180927155139078714395']"));

			for (int i = 0; i < li.size(); i++) {
				System.out.println("The number of radio buttons are" + li.size());

				WebElement radiolabel = li.get(i).findElement(By.tagName("label"));

				System.out.println("inside radio button");
				//radiolabel.click();
				break;
			}
			Thread.sleep(4000);
		} catch (StaleElementReferenceException e) {
			System.out.println(e.getMessage());
		} catch (NoSuchElementException e) {
			System.out.println(e.getMessage());
		} catch (ElementNotVisibleException e) {
			System.out.println(e.getMessage());
		}

	}
	
	public void enterAllocationAreaConsentDate(){
		
		try {

			WebElement elem = driver.findElement(By.id("AlloctnAreaConsentDate"));
			if (elem.isDisplayed())
				elem.sendKeys(Utilities.getCurrentDate()); // Date;
		}

		catch (NoSuchElementException e) {
			System.out.println(e.getMessage());
		} catch (ElementNotVisibleException e) {
			System.out.println(e.getMessage());
		} catch (StaleElementReferenceException e) {
			System.out.println(e.getMessage());
		}
	}

	public void enterDateForMinistersConsent() {

		try {

			WebElement elem = driver.findElement(By.xpath("//input[@data-test-id='20180927155139078714395']"));
			if (elem.isDisplayed())
				elem.sendKeys(Utilities.getCurrentDate()); // Date;
		}

		catch (NoSuchElementException e) {
			System.out.println(e.getMessage());
		} catch (ElementNotVisibleException e) {
			System.out.println(e.getMessage());
		} catch (StaleElementReferenceException e) {
			System.out.println(e.getMessage());
		}

	}

	public void isPartOfGroupNo() {

		driver.findElement(By.xpath("//input[@id='IsPartOfGroupNO']/../label")).click();

	}

	public void isPartOfGroupYes() {

		driver.findElement(By.id("IsPartOfGroupYES")).click();

	}
}
