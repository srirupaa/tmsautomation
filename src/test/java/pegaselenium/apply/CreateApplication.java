package pegaselenium.apply;


import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Properties;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.Select;

import pegaselenium.utils.DriverPage;


public class CreateApplication extends DriverPage{

	public CreateApplication(WebDriver driver) {
		super(driver);
		// TODO Auto-generated constructor stub
	}

	static InputStream in = null;
	String abnText = null;
	String filename = "C:\\TMSAutomation\\src\\main\\java\\resources\\config.properties";



	public void clickApplicationFromMenu() throws InterruptedException {

		driver.findElement(By.xpath(".//*[@id='appview-nav-toggle-one']")).click();
		Thread.sleep(2000);
		WebElement menu = driver.findElement(By.xpath(".//span[@class='menu-item-title']"));
		menu.click();

		Thread.sleep(2000);
		Actions actions = new Actions(driver);
		// actions.moveToElement(menu).perform();
		Thread.sleep(2000);
		driver.findElement(By.linkText("Application")).click();

		Thread.sleep(5000);
		driver.switchTo().frame(1);
	}

	public void returnNewAppFrame() {

		// driver.switchTo().frame("PegaGadget3Ifr");

		driver.switchTo().frame(1);
	}

	public void selectApplicationType(String appType) {
		// returnNewAppFrame();
		 Select transactionDropdown = new Select(driver.findElement(By.id("TransactionType")));
		//Select transactionDropdown = new Select(driver.findElement(By.id("ApplicationType")));

		try {
			Thread.sleep(4000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		transactionDropdown.selectByValue(appType);
	}

	public void selectAuthorityType(String authType) throws InterruptedException {

		 Select applicationTypeDropdown = new Select(driver.findElement(By.id("ApplicationType")));
		//Select applicationTypeDropdown = new Select(driver.findElement(By.id("AuthorityType")));
		Thread.sleep(2000);
		applicationTypeDropdown.selectByValue(authType);
	}

	public void selectTerm(String duration) throws InterruptedException {
		Thread.sleep(5000);
		Select term = new Select(driver.findElement(By.id("Duration")));

		term.selectByValue(duration);
	}

	public void clickDone() {
		driver.findElement(By.name("pyNewCaseMain_pyWorkPage_8")).click();
	}



	/*public void clickContinue() {
		driver.findElement(By.name("pyCaseActionAreaButtons_pyWorkPage_18")).click();
	}*/

	public void clickBackFromPartyDetailsScreen() {
		driver.findElement(By.name("pyCaseActionAreaButtons_pyWorkPage_15")).click();

	}


	


	

	public String CaptureApplicationNumber() throws IOException {
		
		String titleAfterSplit = "";
		try {

			WebElement applicationTitleNumber = driver
					.findElement(By.xpath("//span[@data-test-id='20141009112850013217103']"));
			String titleNumber = applicationTitleNumber.getText();

			System.out.println("The Title created and pending for assessment is--->" + titleNumber);

			try {
				in = new FileInputStream(filename);
			} catch (FileNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			System.out.println("Config file is reached----->");
			Properties prop = new Properties();
			try {
				prop.load(in);
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			in.close();

			FileOutputStream out = new FileOutputStream(filename);

			int titleStartIndex = titleNumber.indexOf("(");

			titleAfterSplit = titleNumber.substring(titleStartIndex + 1, titleNumber.length() - 1);
			prop.setProperty("Title", titleAfterSplit);

			prop.store(out, null);
			
			

		} catch (StaleElementReferenceException e) {
			e.printStackTrace();
		}
		return titleAfterSplit;
	}

}
