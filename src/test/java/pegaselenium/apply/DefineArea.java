package pegaselenium.apply;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.Select;

import pegaselenium.utils.DriverPage;

public class DefineArea extends DriverPage{

	public DefineArea(WebDriver driver) {
		super(driver);
		// TODO Auto-generated constructor stub
	}

	public void clickAddBlocksAndUnits(String mapName, String blocks,String units) throws InterruptedException {
		driver.findElement(By.xpath("//button[contains(.,'Add Blocks and Units')]")).click(); // Define
																								// Area
																								// screen

		Thread.sleep(4000);
		Select selectNameOfMapSheet = new Select(driver.findElement(By.id("SearchBy"))); // selecting
																							// blocks
																							// and
																							// units
		selectNameOfMapSheet.selectByVisibleText(mapName);

		Thread.sleep(4000);
		driver.findElement(By.id("SearchNumber")).sendKeys(blocks);

		Thread.sleep(2000);
		
		if (units.contains("All"))

			driver.findElement(By.xpath("//button[contains(.,'Check All')]")).click();
		
		else if (units.contains("a"))
			driver.findElement(By.id("pySelected1")).click();
		else if (units.contains("b"))
			driver.findElement(By.id("pySelected2")).click();
		else if (units.contains("c"))
			driver.findElement(By.id("pySelected3")).click();
		else if (units.contains("d"))
			driver.findElement(By.id("pySelected4")).click();
		else if (units.contains("e"))
			driver.findElement(By.id("pySelected5")).click();
		else if (units.contains("f"))
			driver.findElement(By.id("pySelected6")).click();
		else if (units.contains("g"))
			driver.findElement(By.id("pySelected7")).click();
		
		else if (units.contains("h"))
			driver.findElement(By.id("pySelected8")).click();
		else if (units.contains("m"))
			driver.findElement(By.id("pySelected12")).click();
		else if (units.contains("j"))
			driver.findElement(By.id("pySelected")).click();
		else if (units.contains("k"))
			driver.findElement(By.id("pySelected10")).click();
		else if (units.contains("l"))
			driver.findElement(By.id("pySelected11")).click();
		else if (units.contains("n"))
			driver.findElement(By.id("pySelected13")).click();
		else if (units.contains("o"))
			driver.findElement(By.id("pySelected14")).click();
		else if (units.contains("p"))
			driver.findElement(By.id("pySelected15")).click();
		
		else if (units.contains("q"))
			driver.findElement(By.id("pySelected16")).click();
		else if (units.contains("r"))
			driver.findElement(By.id("pySelected17")).click();
		else if (units.contains("s"))
			driver.findElement(By.id("pySelected18")).click();
		else if (units.contains("t"))
			driver.findElement(By.id("pySelected19")).click();
		else if (units.contains("u"))
			driver.findElement(By.id("pySelected20")).click();
		else if (units.contains("v"))
			driver.findElement(By.id("pySelected21")).click();
		else if (units.contains("w"))
			driver.findElement(By.id("pySelected22")).click();
		else if (units.contains("x"))
			driver.findElement(By.id("pySelected23")).click();
		else if (units.contains("y"))
			driver.findElement(By.id("pySelected24")).click();
		else if (units.contains("z"))
			driver.findElement(By.id("pySelected25")).click();
		
		Thread.sleep(2000);

		driver.findElement(By.id("ModalButtonSubmit")).click(); // Add button in
																// blocks and
																// units

		Thread.sleep(2000);
	}
}
