package pegaselenium.apply;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.Select;

public class NativeTitleApplication {

	static InputStream in = null;

	public static void main(String[] args) throws InterruptedException, IOException {
		// TODO Auto-generated method stub
		
	
		System.setProperty("webdriver.chrome.driver","C:\\Selenium\\chromedriver.exe");
	
        WebDriver driver = new ChromeDriver();
		

		
		
		//SIT
		driver.get("https://nswpe-sit1.pegacloud.io/prweb/beEBp4uRVTogorRwSwWqbOtn9IL2fwdI*/!STANDARD");
		driver.manage().window().maximize();
		
		

		
		//SIT
		driver.findElement(By.id("txtUserID")).sendKeys("QA1_TitlesAndCustomerOps_User");
		driver.findElement(By.id("txtPassword")).sendKeys("Test@1233");
		driver.findElement(By.id("sub")).click();
		
		
//		
		//dev
//		driver.findElement(By.id("txtUserID")).sendKeys("TMS_TitlesandCustomerOptUser");
//		Thread.sleep(2000);
//		driver.findElement(By.id("txtPassword")).sendKeys("Dev@1234");
//		Thread.sleep(2000);
//		driver.findElement(By.id("sub")).click();
		
		System.out.println("login is clicked");
		Thread.sleep(2000);
		String title = driver.getTitle();
		System.out.println("Here is the title"+title);
		// when clicked New
		
	
		driver.findElement(By.xpath(".//*[@id='appview-nav-toggle-one']")).click();
		Thread.sleep(2000);
		WebElement menu = driver.findElement(By.xpath(".//span[@class='menu-item-title']"));
				menu.click();
		
			Thread.sleep(2000);	
		Actions actions= new Actions(driver);
		actions.moveToElement(menu).perform();
		driver.findElement(By.linkText("Application")).click();
		
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
	
		Thread.sleep(4000); // uncomment this
		int nframes = driver.findElements(By.tagName("iframe")).size();
		
		System.out.println("The iframes are"+nframes);
		
		
		driver.switchTo().frame(1);


		
		System.out.println("in iframe");
		


		
		 Select transactionDropdown = new Select(driver.findElement(By.id("TransactionType")));
		 //Thread.sleep(4000);
		 transactionDropdown.selectByValue("New Application");
		 
		 Select applicationTypeDropdown = new Select(driver.findElement(By.id("ApplicationType")));
		 Thread.sleep(2000);
		 applicationTypeDropdown.selectByValue("Exploration");
		 
		 Select term = new Select(driver.findElement(By.id("Duration")));
		 Thread.sleep(2000);
		 term.selectByValue("2");
		
		//tTdd.click();
		Thread.sleep(2000);
		
		
		driver.findElement(By.name("pyNewCaseMain_pyWorkPage_8")).click();
		
		
		

		
		// Application Details
		
		Select licenseTypeDropdown = new Select(driver.findElement(By.id("LicenseType")));
		 Thread.sleep(2000);
		 licenseTypeDropdown.selectByValue("Standard");
		 Thread.sleep(4000);
		 
			 
		 
		 try{
			List<WebElement> nativeTitleOptionsRadioList= driver.findElements(By.xpath(".//label[starts-with(@for,'NativeTitles')]"));
			 System.out.println("The size of na options is ----->"+nativeTitleOptionsRadioList.size());
			 
			 for(int i=0;i<nativeTitleOptionsRadioList.size();i++){
				 
				 System.out.println("inside radiobuttons list");
				 
				 if(!nativeTitleOptionsRadioList.get(i).isSelected())
				 nativeTitleOptionsRadioList.get(4).click();
				 
			 }
		 }catch(StaleElementReferenceException e)
		 {
			 
			System.out.println(e.getMessage());
		 }
			 
	
		 
		 
		 
		 try{
	
	List<WebElement> li = driver.findElements(By.xpath("//input[@type='checkbox']"));
	
	for(int i = 0;i<li.size();i++)
	{
		System.out.println("the number of checkboxes are "+li.size());
		if(!li.get(i).isSelected())
		{
		li.get(i).click();
		}
		Thread.sleep(4000);
	}
		 }catch(StaleElementReferenceException e)
		 {
			 System.out.println(e.getMessage());
		 }
		 Thread.sleep(2000);
	driver.findElement(By.name("pyCaseActionAreaButtons_pyWorkPage_18")).click();
	
	Thread.sleep(4000);
	
	//party details screen
	
	
	
	WebElement selectPartyTextBox = driver.findElement(By.id("Value"));
	selectPartyTextBox.click();
	selectPartyTextBox.sendKeys("test");
	
	Thread.sleep(2000);
	

	
	List<WebElement> matches = driver.findElements(By.xpath("//span[@class='match-highlight']"));
	System.out.println("The matches for the sendkeys are ---->"+matches.size());
	matches.get(0).click();

Thread.sleep(4000);

driver.findElement(By.xpath("//button[contains(.,'Add Party')]")).click();



Thread.sleep(4000);
//adding party for application contact
WebElement selectPartyTextBox2 = driver.findElement(By.id("Value"));
selectPartyTextBox2.click();
selectPartyTextBox2.sendKeys("test");

Thread.sleep(2000);


List<WebElement> matches2 = driver.findElements(By.xpath("//span[@class='match-highlight']"));
System.out.println("The matches for the sendkeys are ---->"+matches2.size());
matches2.get(1).click();

Thread.sleep(4000);
driver.findElement(By.xpath("//button[contains(.,'Add Party')]")).click();

Thread.sleep(4000);
Select selectParty = new Select(driver.findElement(By.id("Type1")));
selectParty.selectByValue("PrimaryApplicant");

Thread.sleep(4000);
Select selectParty2 = new Select(driver.findElement(By.id("Type2")));

selectParty2.selectByValue("Application Contact");

Thread.sleep(4000);

driver.findElement(By.name("pyCaseActionAreaButtons_pyWorkPage_18")).click(); //Continue button in Party Details screen

Thread.sleep(4000);

driver.findElement(By.xpath("//button[contains(.,'Add Blocks and Units')]")).click(); //Define Area screen

Thread.sleep(4000);
Select selectNameOfMapSheet = new Select(driver.findElement(By.id("SearchBy"))); //selecting blocks and units
selectNameOfMapSheet.selectByIndex(1);

Thread.sleep(4000);
driver.findElement(By.id("SearchNumber")).sendKeys("323");

Thread.sleep(2000);


driver.findElement(By.xpath("//button[contains(.,'Check All')]")).click();
Thread.sleep(2000);

driver.findElement(By.id("ModalButtonSubmit")).click(); //Add button in blocks and units

Thread.sleep(2000);

driver.findElement(By.name("pyCaseActionAreaButtons_pyWorkPage_18")).click(); // Continue button in Define Area

Thread.sleep(2000);

driver.findElement(By.name("pyCaseAttachments_pyWorkPage_58")).click();

Thread.sleep(2000);



Actions actionsAttachment= new Actions(driver);
actionsAttachment.moveToElement(driver.findElement(By.name("pyCaseAttachments_pyWorkPage_58"))).sendKeys(Keys.ENTER).perform();


//Attaching file from device

Thread.sleep(2000);

driver.findElement(By.id("$PpyAttachmentPage$ppxAttachName")).sendKeys("C:\\Users\\bejjarv\\Desktop\\Test.txt");

Thread.sleep(2000);

Select fileCategory = new Select(driver.findElement(By.name("$PdragDropFileUpload$ppxResults$l1$ppyCategory")));
fileCategory.selectByValue("SubmittedApplication");

Thread.sleep(2000);

driver.findElement(By.id("ModalButtonSubmit")).click();

Thread.sleep(2000);
driver.findElement(By.name("pyCaseActionAreaButtons_pyWorkPage_18")).click();

Thread.sleep(2000);
driver.findElement(By.id("DeclarationConfirmation")).click();

Thread.sleep(2000);
driver.findElement(By.name("pyCaseActionAreaButtons_pyWorkPage_19")).click();


Thread.sleep(4000);

// capturing Title value 
try{

WebElement applicationTitleNumber = driver.findElement(By.xpath("//span[@data-test-id='20141009112850013217103']"));
String titleNumber = applicationTitleNumber.getText();

System.out.println("The Title created and pending for assessment is--->"+titleNumber);



String filename = "C:\\TMSAutomation\\src\\main\\java\\resources\\config.properties";

try {
	in = new FileInputStream(filename);
} catch (FileNotFoundException e) {
	// TODO Auto-generated catch block
	e.printStackTrace();
}
System.out.println("Config file is reached----->");
Properties prop = new Properties();
try {
	prop.load(in);
} catch (IOException e) {
	// TODO Auto-generated catch block
	e.printStackTrace();
}
in.close();

FileOutputStream out = new FileOutputStream(filename);


int titleStartIndex = titleNumber.indexOf("(");

String titleAfterSplit = titleNumber.substring(titleStartIndex+1, titleNumber.length()-1);
prop.setProperty("Title", titleAfterSplit);

prop.store(out, null);

}catch(StaleElementReferenceException e){
	e.printStackTrace();
}

































	








	






	
	
		

		
		
		
		
		
		
		}
		
	
		
		

}
