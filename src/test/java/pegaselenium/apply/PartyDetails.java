package pegaselenium.apply;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;

import pegaselenium.utils.DriverPage;

public class PartyDetails extends DriverPage{

	public PartyDetails(WebDriver driver) {
		super(driver);
		// TODO Auto-generated constructor stub
	}
	public void selectPartyOneFromTextBox() throws InterruptedException {
		WebElement selectPartyTextBox = driver.findElement(By.id("Value"));
		selectPartyTextBox.click();
		selectPartyTextBox.sendKeys("test");
		Thread.sleep(2000);
		List<WebElement> matches = driver.findElements(By.xpath("//span[@class='match-highlight']"));
		System.out.println("The matches for the sendkeys are ---->" + matches.size());
		matches.get(0).click();
	}

	public void clickAddParty() {
		driver.findElement(By.xpath("//button[contains(.,'Add Party')]")).click();
	}

	public void selectPartyFromTextBox(int index, String name) throws InterruptedException {
		WebElement selectPartyTextBox2 = driver.findElement(By.id("Value"));
		selectPartyTextBox2.click();
		selectPartyTextBox2.sendKeys(name);

		Thread.sleep(2000);

		List<WebElement> matches2 = driver.findElements(By.xpath("//span[@class='match-highlight']"));
		System.out.println("The matches for the sendkeys are ---->" + matches2.size());
		
		try{matches2.get(index).click();}
		catch(IndexOutOfBoundsException e){}
	}

	public void selectRole1(String role) {

		Select role1 = new Select(driver.findElement(By.id("Type1")));

		role1.selectByVisibleText(role);
	}

	public void selectRole2(String role) {
		Select role2 = new Select(driver.findElement(By.id("Type2")));

		role2.selectByVisibleText(role);
	}

	public void selectRole3(String role) {
		Select role2 = new Select(driver.findElement(By.id("Type3")));

		role2.selectByVisibleText(role);
	}


}
