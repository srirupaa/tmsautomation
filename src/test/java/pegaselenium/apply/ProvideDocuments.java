package pegaselenium.apply;

import java.io.File;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.Select;

import pegaselenium.utils.DriverPage;

public class ProvideDocuments extends DriverPage{


	public ProvideDocuments(WebDriver driver) {
		super(driver);
		// TODO Auto-generated constructor stub
	}

	public void clickAddAttachment(String fileN) throws InterruptedException {

		driver.findElement(By.name("pyCaseAttachments_pyWorkPage_58")).click();

		Thread.sleep(2000);

		Actions actionsAttachment = new Actions(driver);
		actionsAttachment.moveToElement(driver.findElement(By.name("pyCaseAttachments_pyWorkPage_58")))
				.sendKeys(Keys.ENTER).perform();

		// Attaching file from device

		Thread.sleep(2000);
		File fi = new File(fileN);
		
		String abFilePath = fi.getAbsolutePath();

		driver.findElement(By.id("$PpyAttachmentPage$ppxAttachName")).sendKeys(abFilePath);

		Thread.sleep(2000);

	}

	public void selectFileCategory(String category) {
		Select fileCategory = new Select(driver.findElement(By.name("$PdragDropFileUpload$ppxResults$l1$ppyCategory")));
		fileCategory.selectByVisibleText(category);
	}

	public void clickAttachInAttachFile() {
		driver.findElement(By.id("ModalButtonSubmit")).click();

	}
}
