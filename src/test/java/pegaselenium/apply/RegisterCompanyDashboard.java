package pegaselenium.apply;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.Select;
import org.testng.annotations.Test;

public class RegisterCompanyDashboard {
	
	WebDriver driver;
	static InputStream in = null;
	String abnText = null;
	
	public RegisterCompanyDashboard(WebDriver driver){
		this.driver=driver;
		
		
	}
	

	public void typeUserName(){
		driver.findElement(By.id("txtUserID")).sendKeys("QA1_TitlesAndCustomerOps_User");
	}
	public void typePassword(){
		driver.findElement(By.id("txtPassword")).sendKeys("Test@1233");
	}
	
	public void clickSubmit(){
		
		driver.findElement(By.id("sub")).click();
		
	}
		
public void clickRegisterCompanyMenu(){
		driver.findElement(By.xpath(".//*[@id='appview-nav-toggle-one']")).click();
	
		WebElement menu = driver.findElement(By.xpath(".//span[@class='menu-item-title']"));
			menu.click();

		Actions actions= new Actions(driver);
		actions.moveToElement(menu).perform();
		driver.findElement(By.linkText("Register Company")).click();
			 
		int nframes = driver.findElements(By.tagName("iframe")).size();
		
		System.out.println("The iframes are"+nframes);
		

		driver.switchTo().frame(1);
}
	public void searchByName() throws InterruptedException{	
		try{
	Select selectBusinessBy = new Select(driver.findElement(By.id("SearchBusinessBy")));
	selectBusinessBy.selectByValue("Name");
	
	Thread.sleep(2000);
	
WebElement textForSearch=	driver.findElement(By.xpath(".//input[@data-test-id='2018062813513504957960']"));

	
	Actions searchText= new Actions(driver);
	searchText.moveToElement(textForSearch).sendKeys("Test").sendKeys(Keys.TAB).perform();
	
Thread.sleep(4000);
	
	driver.findElement(By.xpath(".//*[@data-test-id='2018062813520800367373']")).click();
	
	
	Thread.sleep(4000);
	
int framesForReview = driver.findElements(By.tagName("iframe")).size();
	
	System.out.println("The iframes are"+framesForReview);
	

	Thread.sleep(4000);
	
	WebElement tableCompanyNames= driver.findElement(By.id("bodyTbl_right"));
		
	
	List<WebElement> rowsForTableCompanyNames = tableCompanyNames.findElements(By.tagName("tr"));
	System.out.println("The total number of values for company name are--->"+rowsForTableCompanyNames.size());
	
	for(int i =0;i<rowsForTableCompanyNames.size();i++){
		
		
	
	List<WebElement> columnsForTableCompanyNames=	rowsForTableCompanyNames.get(2).findElements(By.xpath(".//span[@data-test-id='2016072109335505834280']"));
	
	
		for(int j=0;j<columnsForTableCompanyNames.size();j++){
			
			columnsForTableCompanyNames.get(0).click();
		}
		
		
	}
	
	
	}
catch(StaleElementReferenceException e){
		
		e.getMessage();
	}
	
	}
	// by ABN
	public void searchByABN() throws InterruptedException{
		
		
	Select selectBusinessBy = new Select(driver.findElement(By.id("SearchBusinessBy")));
	selectBusinessBy.selectByValue("ABN");
	
	Thread.sleep(4000);
	
abnText = driver.findElement(By.id("EIN")).getAttribute("value");
	
	System.out.println("The ABN value is---->"+abnText);
	
	
	Thread.sleep(4000);
	
WebElement textForSearch=	driver.findElement(By.xpath(".//input[@data-test-id='2018062813513504957960']"));
	
	Actions searchText= new Actions(driver);
	searchText.moveToElement(textForSearch).sendKeys(abnText).sendKeys(Keys.TAB).perform();
	
Thread.sleep(4000);
	
	driver.findElement(By.xpath(".//*[@data-test-id='2018062813520800367373']")).click();
	
	
	Thread.sleep(4000);
	
	driver.findElement(By.xpath(".//span[@data-test-id='2016072109335505834280']")).click();
	
	Thread.sleep(4000);
	
	
	System.out.println("ABN search completed");

	}
	
	//end of ABN
	
	// by ACN 004 700 485
	
	public void searchByACN() throws InterruptedException{
	
	
WebElement textForSearchACN = driver.findElement(By.xpath(".//input[@data-test-id='2018062813513504957960']"));

	
	Actions searchTextACN= new Actions(driver);
	searchTextACN.moveToElement(textForSearchACN).click().sendKeys("004 700 485").sendKeys(Keys.TAB).perform();
	
Thread.sleep(4000);
	
	driver.findElement(By.xpath(".//*[@data-test-id='2018062813520800367373']")).click();
	
	
	Thread.sleep(4000);
	
	driver.findElement(By.xpath(".//span[@data-test-id='2016072109335505834280']")).click();
	
	Thread.sleep(2000);
	
	}
	//end of ACN
	

	public void enterAutoStreetAddress() throws InterruptedException{
		WebElement AddressTextBox = driver.findElement(By.id("AddressStringFromLookup"));
		AddressTextBox.click();
		AddressTextBox.sendKeys("str");
		try {
			Thread.sleep(4000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		List<WebElement> addressLookupMatches = driver.findElements(By.xpath("//span[@class='match-highlight']"));
		System.out.println("The matches for the sendkeys are ---->"+addressLookupMatches.size());
		addressLookupMatches.get(0).click();
		Thread.sleep(4000);
		}
		
		public void enterAutoPostalAddress() throws InterruptedException{
			
			WebElement AddressTextBoxForPostal = driver.findElement(By.name("$PpyWorkPage$pCompanyDetails$pAddresses$gPostal$pAddressStringFromLookup"));
			                                                                 
			AddressTextBoxForPostal.click();
			AddressTextBoxForPostal.sendKeys("str");
			
			try{
				Thread.sleep(4000);
			}catch (InterruptedException e){
				
				e.printStackTrace();
			}
			List<WebElement> addressLookupMatchesPostal = driver.findElements(By.xpath("//span[@class='match-highlight']"));
			System.out.println("The matches for the sendkeys are ---->"+addressLookupMatchesPostal.size());
			addressLookupMatchesPostal.get(0).click();
			Thread.sleep(4000);
			
					
			
		}
		
		public void selectCheckBoxSameAddressForPostalAddress() throws InterruptedException{
			driver.findElement(By.id("IsRequired")).click();
			
			Thread.sleep(4000);
			}
		
		
		// for Street Address
				public void clickNotFoundForStreetAddress()
				{
					
					driver.findElement(By.xpath(".//button[@data-test-id='20180628180755054819791']")).click();
				}
				
				public void inputStreetAddressLine1(){
					
					driver.findElement(By.id("AddressLine1")).sendKeys("highpoint");
					
				}
				
				public void inputStreetAddressLine2(){
					
					driver.findElement(By.id("AddressLine2")).sendKeys("323 marybynong");
				}
				
				public void cityForStreetAddress(){
					
					driver.findElement(By.id("pyCity")).sendKeys("Melbourne");
					
				}
				
				public void postalCodeForStreetAddress(){
					driver.findElement(By.id("pyPostalCode")).sendKeys("3100");
				
				}
				
				public void stateForStreetAddress(){
					driver.findElement(By.id("pyState")).sendKeys("Victoria");
				}
				
				public void countryForStreetAddress(){
					
					driver.findElement(By.id("pyCountry")).sendKeys("Australia");
				}
			// end of Street address
		
				
				// for postal address
				public void clickNotFoundForPostalAddress()
				{
					
					driver.findElement(By.name("CompanyAddressPostal_pyWorkPage.CompanyDetails.Addresses(Postal)_4")).click();
				}
				
				public void inputPostalAddressLine1(){
					
					driver.findElement(By.name("$PpyWorkPage$pCompanyDetails$pAddresses$gPostal$pAddressLine1")).sendKeys("highpoint");
					
				}
				
				public void inputPostalAddressLine2(){
					
					driver.findElement(By.name("$PpyWorkPage$pCompanyDetails$pAddresses$gPostal$pAddressLine2")).sendKeys("323 marybynong");
				}
				
				public void cityPostalAddress(){
					
					driver.findElement(By.name("$PpyWorkPage$pCompanyDetails$pAddresses$gPostal$ppyCity")).sendKeys("Melbourne");
					
				}
				
				public void postalCodeForPostalAddress(){
					driver.findElement(By.name("$PpyWorkPage$pCompanyDetails$pAddresses$gPostal$ppyPostalCode")).sendKeys("3100");
				
				}
				
				public void stateForPostalAddress(){
					driver.findElement(By.name("$PpyWorkPage$pCompanyDetails$pAddresses$gPostal$ppyState")).sendKeys("Victoria");
				}
				
				public void countryForPostalAddress(){
					
					driver.findElement(By.name("$PpyWorkPage$pCompanyDetails$pAddresses$gPostal$ppyCountry")).sendKeys("Australia");
				}
				
				
				//for postal address
		
	
	
	public void inputTASApplicantID(){
	driver.findElement(By.id("EntityID")).sendKeys("12345");
	}
	

	public void inputTASContactID(){
	driver.findElement(By.id("ID")).sendKeys("54321");
	}
	
	
	public void clickSubmitCompany(){
	
    driver.findElement(By.xpath("//button[contains(.,'Submit')]")).click();
	}

    public void clickContinue(){
    driver.findElement(By.name("AcknowledgeRegistration_pyWorkPage_5")).click();

    }

	
	
	
	
	

	

	
}
		



