/**
 * 
 */
package pegaselenium.apply;

import static org.testng.Assert.assertEquals;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.Select;

/**
 * @author bejjarv
 *
 */
public class RegisterIndividualDashboard {
	
	WebDriver driver;
//	
//	By appview = By.xpath(".//*[@id='appview-nav-toggle-one']");
//	
//	By menu = By.xpath(".//span[@class='menu-item-title']");
//	//		menu.click();
//	//driver.findElement(By.xpath(".//ul[@id='menu-1543533540644']")).click();
//	//	Thread.sleep(2000);	
//	Actions actions= new Actions(driver);
//	actions.moveToElement(menu).perform();
//	driver.findElement(By.linkText("Application")).click();
//
//	/**
//	 * @param args
//	 */
//	public RegisterIndividualDashboard(WebDriver driver)
//	{
//		
//		this.driver = driver;
//	}
//	
//	/**
//	 * The following method is for Register Individual from Dashboard
//	 * 
//	 */
//	
//	public void selectRegisterIndividual(){
//		
//		driver.findElement(appview).click();
//WebElement menu=		driver.findElement((By) menu).click();
//		
//		
//		
//		
//	}
	
	static InputStream in = null;
//	WebDriver driver = new ChromeDriver();
	
	public RegisterIndividualDashboard(WebDriver driver){
		
		this.driver =driver;
	}
	//public static void main(String [] args) throws InterruptedException, IOException{
	
//		public void setChromeDriver(){
//	System.setProperty("webdriver.chrome.driver","C:\\Users\\bejjarv\\Downloads\\chromedriver_win32(2)\\chromedriver.exe");
//	
//	
//		}
		
	
//		public void getURL(){
//		//SIT
//	driver.get("https://nswpe-sit1.pegacloud.io/prweb/beEBp4uRVTogorRwSwWqbOtn9IL2fwdI*/!STANDARD");
//		driver.manage().window().maximize();
//	//	driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
//		
//		}

		
		//SIT
		
		public void typeUserName() {
		driver.findElement(By.id("txtUserID")).sendKeys("QA1_TitlesAndCustomerOps_User");
		}
		public void typePassword(){
		driver.findElement(By.id("txtPassword")).sendKeys("Test@1233");
		}
		public void clickSubmit(){
		driver.findElement(By.id("sub")).click();
		

		}
		
//		}
		//dev
//		driver.findElement(By.id("txtUserID")).sendKeys("TMS_TitlesandCustomerOptUser");
//		Thread.sleep(2000);
//		driver.findElement(By.id("txtPassword")).sendKeys("Dev@1234");
//		Thread.sleep(2000);
//		driver.findElement(By.id("sub")).click();
		
	
		
//		String title = driver.getTitle();
//		System.out.println("Here is the title"+title);
//	
//		
//	
		public void clickRegisterIndividualMenu() {
		driver.findElement(By.xpath(".//*[@id='appview-nav-toggle-one']")).click();
	
		WebElement menu = driver.findElement(By.xpath(".//span[@class='menu-item-title']"));
			menu.click();
//	
//			Thread.sleep(2000);	
		Actions actions= new Actions(driver);
		actions.moveToElement(menu).perform();
		driver.findElement(By.linkText("Register Individual")).click();
			 // uncomment this
		int nframes = driver.findElements(By.tagName("iframe")).size();
		
		System.out.println("The iframes are"+nframes);
		

		driver.switchTo().frame(1);

		}
		
		public void fillFirstNameAndSurname() throws InterruptedException{
//		
//		System.out.println("in iframe");
//		
//		
	driver.findElement(By.id("FirstName")).sendKeys("TestAutoRegIndividual");
		
Thread.sleep(4000);
	try{
	
		Actions surname= new Actions(driver);
		surname.moveToElement(driver.findElement(By.id("FirstName"))).sendKeys(Keys.TAB).perform();
		
		Actions surnameTextBox= new Actions(driver);
		surnameTextBox.moveToElement(driver.findElement(By.id("LastName"))).sendKeys("TestAuto").perform();
		
	}catch(StaleElementReferenceException e){
			e.getMessage();
	}
		}
//
//		Thread.sleep(4000);
		public void enterMobile(){
		driver.findElement(By.id("Carrier")).sendKeys("1234567890");
		}
		
		public void enterAutoStreetAddress() throws InterruptedException{
		WebElement AddressTextBox = driver.findElement(By.id("AddressStringFromLookup"));
		AddressTextBox.click();
		AddressTextBox.sendKeys("str");
		try {
			Thread.sleep(4000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		List<WebElement> addressLookupMatches = driver.findElements(By.xpath("//span[@class='match-highlight']"));
		System.out.println("The matches for the sendkeys are ---->"+addressLookupMatches.size());
		addressLookupMatches.get(0).click();
		Thread.sleep(4000);
		}
		
		public void enterAutoPostalAddress() throws InterruptedException{
			
			WebElement AddressTextBoxForPostal = driver.findElement(By.name("$PpyWorkPage$pPersonDetails$pAddresses$gPostal$pAddressStringFromLookup"));
			AddressTextBoxForPostal.click();
			AddressTextBoxForPostal.sendKeys("str");
			
			try{
				Thread.sleep(4000);
			}catch (InterruptedException e){
				
				e.printStackTrace();
			}
			List<WebElement> addressLookupMatchesPostal = driver.findElements(By.xpath("//span[@class='match-highlight']"));
			System.out.println("The matches for the sendkeys are ---->"+addressLookupMatchesPostal.size());
			addressLookupMatchesPostal.get(0).click();
			Thread.sleep(4000);
			
					
			
		}
		
		
		
		
		
		
		
		public void selectCheckBoxSameAddressForPostalAddress() throws InterruptedException{
		driver.findElement(By.id("IsRequired")).click();
		
		Thread.sleep(4000);
		}
		
		public void clickSubmitRegisterIndividual(){
	driver.findElement(By.name("pyCaseActionAreaButtons_pyWorkPage_12")).click();
	}
//		Thread.sleep(4000);
		
		public void verifyAcknowledge(){
		
		WebElement RegIndividualAcknowledgeMessage = driver.findElement(By.xpath(".//div[@data-test-id='2018121413065505098507']"));
		
		String ackText = RegIndividualAcknowledgeMessage.getText();
		
		String expectedText= "Registration completed, Click Continue to proceed further.";
		
		
		if(ackText.equals(expectedText))
			System.out.println("Registration completed successfully");
		
		}
		public void clickContinue() throws InterruptedException{
		driver.findElement(By.name("AcknowledgeRegistration_pyWorkPage_5")).click();
		Thread.sleep(2000);
		
		System.out.println("Continue button clicked successfully");
		}
		
		// for Street Address
		public void clickNotFoundForStreetAddress()
		{
			
			driver.findElement(By.xpath(".//button[@data-test-id='20180628180755054819791']")).click();
		}
		
		public void inputStreetAddressLine1(){
			
			driver.findElement(By.id("AddressLine1")).sendKeys("highpoint");
			
		}
		
		public void inputStreetAddressLine2(){
			
			driver.findElement(By.id("AddressLine2")).sendKeys("323 marybynong");
		}
		
		public void cityForStreetAddress(){
			
			driver.findElement(By.id("pyCity")).sendKeys("Melbourne");
			
		}
		
		public void postalCodeForStreetAddress(){
			driver.findElement(By.id("pyPostalCode")).sendKeys("3100");
		
		}
		
		public void stateForStreetAddress(){
			driver.findElement(By.id("pyState")).sendKeys("Victoria");
		}
		
		public void countryForStreetAddress(){
			
			driver.findElement(By.id("pyCountry")).sendKeys("Australia");
		}
	// end of Street address
		
		
		
		// for Postal Address
		public void clickNotFoundForPostalAddress()
		{
			
			driver.findElement(By.name("PostalAddressDetails_pyWorkPage.PersonDetails.Addresses(Postal)_4")).click();
		}
		
		public void inputPostalAddressLine1(){
			
			driver.findElement(By.name("$PpyWorkPage$pPersonDetails$pAddresses$gPostal$pAddressLine1")).sendKeys("highpoint");
			
		}
		
		public void inputPostalAddressLine2(){
			
			driver.findElement(By.name("$PpyWorkPage$pPersonDetails$pAddresses$gPostal$pAddressLine2")).sendKeys("323 marybynong");
		}
		
		public void cityPostalAddress(){
			
			driver.findElement(By.name("$PpyWorkPage$pPersonDetails$pAddresses$gPostal$ppyCity")).sendKeys("Melbourne");
			
		}
		
		public void postalCodeForPostalAddress(){
			driver.findElement(By.name("$PpyWorkPage$pPersonDetails$pAddresses$gPostal$ppyPostalCode")).sendKeys("3100");
		
		}
		
		public void stateForPostalAddress(){
			driver.findElement(By.name("$PpyWorkPage$pPersonDetails$pAddresses$gPostal$ppyState")).sendKeys("Victoria");
		}
		
		public void countryForPostalAddress(){
			
			driver.findElement(By.name("$PpyWorkPage$pPersonDetails$pAddresses$gPostal$ppyCountry")).sendKeys("Australia");
		}
		
		//end of postal address
		
		//driver.quit();
	
	

}

