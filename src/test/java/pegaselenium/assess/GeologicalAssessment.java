package pegaselenium.assess;

import java.io.FileInputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import pegaselenium.apply.CreateApplication;
import pegaselenium.login.LoginPage;
import pegaselenium.utils.DPETestBase;
import pegaselenium.utils.ExcelUtils;

public class GeologicalAssessment extends DPETestBase {
	String assessor = null;
	String Assessors = null;
	int position = 0;
	static FileInputStream in = null;
	static Properties prop = null;
	LoginPage login;
	CreateApplication createApplication;

	@BeforeTest
	public void setUP() {

		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
		driver.get(env.getBaseURL());

		login = new LoginPage(driver);
		createApplication = new CreateApplication(driver);

	}

	@DataProvider(name = "GeoAssessmentData")
	public Object[][] createData() throws Exception {
		ExcelUtils xUtil = new ExcelUtils("testdata\\TitleData.xlsx");
		Object[][] retObjArr = xUtil.getXLDataByPOI("Assessment Data");
		return (retObjArr);
	}

	@Test(dataProvider = "GeoAssessmentData")
	public void completeGeoAssessment(String assessNo, String comments, String reviewDate) throws InterruptedException, IOException {

		driver.switchTo().defaultContent();	
		if (!login.isGeoUserLoggedIn())
			login.loginAsGeoAssessmentUser();
		Thread.sleep(2000);
		login.navigateToDashboard();
		int framesForReview = driver.findElements(By.tagName("iframe")).size();

		System.out.println("The iframes are" + framesForReview);

		if (framesForReview == 3) {
			driver.switchTo().frame(2);
		} else if (framesForReview == 2) {
			driver.switchTo().frame(1);
		} else {
			driver.switchTo().frame(0);
		}
		// click dropdown to select Geological assessment WB
		try {

			//driver.findElement(By.xpath("//i[contains(@class,'cursordefault icons pi pi-caret-solid-down')]")).click();
			driver.findElement(By.xpath("//span[@id='TMS_GeoAssessmentWB']")).click();
			System.out.println("dropdown is clicked");

		} catch (NoSuchElementException e) {
			System.out.println(e.getMessage());
		}
		Thread.sleep(2000);

		// selecting Geological Assessment Work Basket
	/*	try {
			WebElement wbTable = driver.findElement(By.id("gridLayoutTable"));

			List<WebElement> wbTableList = wbTable.findElements(By.tagName("tr"));

			Thread.sleep(4000);

			for (int j = 0; j < wbTableList.size(); j++) {

				List<WebElement> selectWbGA = wbTableList.get(j).findElements(By.tagName("td"));
				Thread.sleep(4000);
				for (int k = 0; k < selectWbGA.size(); k++) {

					selectWbGA.get(k).findElement(By.xpath("//span[contains(text(),'TMS Geological Assessment WB')]"))
							.click();
				}

			}
		} catch (StaleElementReferenceException e) {

			System.out.println(e.getMessage());
		}
*/
		System.out.println("Wb is selected");

		Thread.sleep(4000);

		int nframes = driver.findElements(By.tagName("iframe")).size();

		System.out.println("The iframes are" + nframes);

		WebElement gaTable = driver.findElement(By.id("bodyTbl_right"));

		Thread.sleep(2000);
		List<WebElement> gaTableRows = gaTable.findElements(By.tagName("tr"));
		Thread.sleep(2000);

		System.out.println("the Size of Rowsin the table ---->" + gaTableRows.size());

		// gets GA from config file which has been dynamically created during
		// Application completeness check

		/*String filename = "C:\\TMSAutomation\\src\\main\\java\\resources\\config.properties";

		in = new FileInputStream(filename);
		System.out.println("Config file is reached----->");
		prop = new Properties();

		prop.load(in);
		String GAAssessor = prop.getProperty("GAAssessor"); // getting
															// RAAssessor from
															// config file for
															// checking in the
															// list
*/
		for (int i = 0; i < gaTableRows.size(); i++) {

			List<WebElement> GATitleColumns = gaTableRows.get(i).findElements(By.tagName("td"));

			for (int j = 0; j < GATitleColumns.size(); j++) {

				if (GATitleColumns.get(1).getText().equalsIgnoreCase(assessNo)) {

					GATitleColumns.get(1).click();
				}

			}

		}

		Thread.sleep(2000);


		driver.switchTo().defaultContent();

			int framesfortextarea = driver.findElements(By.tagName("iframe")).size();

		System.out.println("The iframes for area are" + framesfortextarea);

		if (framesfortextarea == 3) {
			driver.switchTo().frame(2);
		} else if (framesfortextarea == 2) {
			driver.switchTo().frame(1);
		} else {
			driver.switchTo().frame(0);
		}
		
		Assessments asses = new Assessments(driver);
		// AssessmentSummary Text area is clicked

		try{
			WebElement ta = driver.findElement(By.id("AssessmentSummary"));
			ta.click();
			System.out.println("Text area is clicked");
			ta.sendKeys(comments);
		}

		catch(Exception e){
			asses.pressActionsBtn();
			asses.selectProvideAdvice();
			WebElement ta = driver.findElement(By.id("AssessmentSummary"));
			ta.click();
			System.out.println("Text area is clicked");
			ta.sendKeys(comments);
		}

		// select date

		WebElement raAssessmentDate = driver.findElement(By.id("$PpyWorkPage$pAssessment$pAssessmentDateSpan"));
		raAssessmentDate.click();
		Thread.sleep(2000);

		driver.findElement(By.id("todayLink")).click();
		//driver.switchTo().defaultContent();
		Thread.sleep(3000);
		driver.findElement(By.xpath("//button[contains(.,'Submit')]")).click();

		// End of Review

		Thread.sleep(2000);
		driver.switchTo().defaultContent();
		System.out.println("GA assessment completed hurrah!!!!!!!");
		login.navigateToDashboard();
	}

}
