package pegaselenium.assess;

import java.io.FileInputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.Select;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import pegaselenium.apply.CreateApplication;
import pegaselenium.login.LoginPage;
import pegaselenium.utils.DPETestBase;
import pegaselenium.utils.ExcelUtils;

public class TRCRecommendation extends DPETestBase{
	
	
	String assessor = null;
	String Assessors = null;
	int position =0;

	static FileInputStream in = null;
	static Properties prop = null;
	LoginPage login;
	CreateApplication createApplication;
	@BeforeTest
	public void setUP() {

		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
		driver.get(env.getBaseURL());

		login = new LoginPage(driver);
		createApplication = new CreateApplication(driver);

	}

	@DataProvider(name = "RecomData")
	public Object[][] createData() throws Exception {
		ExcelUtils xUtil = new ExcelUtils("testdata\\TitleData.xlsx");
		Object[][] retObjArr = xUtil.getXLDataByPOI("Title Data");
		return (retObjArr);
	}

	@Test(dataProvider = "RecomData")
	public void trcRecommendation(String title, String comments, String reviewDate) throws InterruptedException, IOException {
		
		
		
		login.loginAsTRCRecommendationUser();
		
		Thread.sleep(2000);
		
		driver.manage().window().maximize();
		int framesForReview = driver.findElements(By.tagName("iframe")).size();
		
		System.out.println("The iframes are"+framesForReview);
		
	if(framesForReview==3){
		driver.switchTo().frame(2);
		}else
			if(framesForReview==2)
		{
			driver.switchTo().frame(1);
		}else{
			driver.switchTo().frame(0);
		}
	
	// select drop down
	
		try{
			
			driver.manage().window().maximize();
					
			driver.findElement(By.xpath("//i[contains(@class,'cursordefault icons pi pi-caret-solid-down')]")).click();
					
			System.out.println("dropdown is clicked");
						
		}catch(NoSuchElementException e){
			System.out.println(e.getMessage());
		}
		Thread.sleep(6000);
		
		// TRC Recommendation WB
		
		try{
	WebElement wbTable=	driver.findElement(By.id("gridLayoutTable"));
	
	List<WebElement> wbTableList= wbTable.findElements(By.tagName("tr"));
	
	Thread.sleep(4000);
	
	for(int j =0;j<wbTableList.size();j++){
		
		
	List<WebElement> selectWbTRC  = wbTableList.get(j).findElements(By.tagName("td"));
	Thread.sleep(4000);
	for(int k =0;k<selectWbTRC.size();k++){
		
	
		
		selectWbTRC.get(k).findElement(By.xpath("//span[contains(text(),'TRC Recommendation WB')]")).click();
	}
	
	}
	}
		catch(StaleElementReferenceException e){
			
			System.out.println(e.getMessage());
	
	
	
	
	
	}
	
	System.out.println("Wb is selected");
		
		
		Thread.sleep(4000);
		
		
		
		int nframes = driver.findElements(By.tagName("iframe")).size();
		
		System.out.println("The iframes are"+nframes);
		
		
		
	WebElement trcTable=	driver.findElement(By.id("bodyTbl_right"));
	
	Thread.sleep(2000);
	List<WebElement> trcTableRows = trcTable.findElements(By.tagName("tr"));
	Thread.sleep(2000);
	
	System.out.println("the Size of Rowsin the table ---->"+trcTableRows.size());
	

	// select Title which was dynamically created as part of Create Application
	
String filename = "C:\\TMSAutomation\\src\\main\\java\\resources\\config.properties";
	
	in = new FileInputStream(filename);
	System.out.println("Config file is reached----->");
	prop = new Properties();
	
	prop.load(in);
String TRCRecommendationAssessor=	prop.getProperty("Title"); // getting Title from config file for checking in the list

	
	
	
	for(int i = 0;i<trcTableRows.size();i++){
		
		List<WebElement> TRCTitleColumns = trcTableRows.get(i).findElements(By.tagName("td"));
		
		for(int j = 0;j<TRCTitleColumns.size();j++)
		{
		

			if(TRCTitleColumns.get(1).getText().equalsIgnoreCase(TRCRecommendationAssessor)){
				
				TRCTitleColumns.get(1).click();
			}
	
		}
	
		
	}	
	
	Thread.sleep(2000);
	

	
	Thread.sleep(4000);
	driver.manage().window().maximize();
	System.out.println("window is maximized");
	
	Thread.sleep(4000);
	
	
	driver.switchTo().defaultContent();
	

	
int framesfortextarea = driver.findElements(By.tagName("iframe")).size();
	
	System.out.println("The iframes for area are"+framesfortextarea);
	
if(framesfortextarea==3){
	driver.switchTo().frame(2);
	}else
		if(framesfortextarea==2)
	{
		driver.switchTo().frame(1);
	}else{
		driver.switchTo().frame(0);
	}


	
	Thread.sleep(4000);
	
	// AssessmentSummary Text area is clicked

	WebElement ta = driver.findElement(By.id("AssessmentSummary"));
		
	ta.click();
	
	System.out.println("Text area is clicked");
	
	//Enter comments
	
	ta.sendKeys("Reviewed");
	
	Thread.sleep(2000);
	
	// select GRANT from applicationOutcome
	
	Select applicationOutcome = new Select(driver.findElement(By.id("TRCDecision")));
	applicationOutcome.selectByValue("GRANT");
	
	Thread.sleep(4000);
	
	SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy");
	String applicationSubmittedDate = format.format(new Date());
		
		
		System.out.println("The Date is --->"+applicationSubmittedDate);


Thread.sleep(4000);

	

	
	// select Date
	
	WebElement raAssessmentDate =driver.findElement(By.xpath("//*[@data-test-id='201809191415100906476007a6-aed2-40e6-bc17-6529c3e230a2333-DatePicker']"));
	raAssessmentDate.click();
	Thread.sleep(2000);
	
	driver.findElement(By.id("todayLink")).click();
	
	Thread.sleep(3000);
	driver.findElement(By.xpath("//button[contains(.,'Submit')]")).click();

	//End of Review

	Thread.sleep(6000);
	
	System.out.println("TRC Recommendations assessment completed hurrah!!!!!!!");
	
	
	Thread.sleep(6000);
	
	try{
		WebElement actionsButton = driver.findElement(By.xpath("//button[contains(.,'Actions')]"));
	
	actionsButton.click();

	Thread.sleep(2000);

	Actions reviewApplication= new Actions(driver);
	reviewApplication.moveToElement(driver.findElement(By.xpath("//button[contains(.,'Actions')]"))).sendKeys(Keys.ENTER).perform();
	System.out.println("Refresh is clicked.............");
	}catch(NoSuchElementException e){
		System.out.println(e.getMessage());
	}catch(StaleElementReferenceException e){
		System.out.println(e.getMessage());
	}

	Thread.sleep(6000);


	int framesInAssess = driver.findElements(By.tagName("iframe")).size();

	Thread.sleep(2000);

	System.out.println("The iframes in Assess are"+framesInAssess);
	if(framesInAssess==3){
		driver.switchTo().frame(2);
		}else
			if(framesInAssess==2)
		{
			driver.switchTo().frame(1);
		}else{
			driver.switchTo().frame(0);
		}

	

	Thread.sleep(6000);
		
		
		
		

	}

}
