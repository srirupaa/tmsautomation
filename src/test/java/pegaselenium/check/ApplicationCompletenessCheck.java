package pegaselenium.check;

import java.util.List;
import java.util.NoSuchElementException;
import java.util.Properties;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.Test;

import pegaselenium.assess.RegulatoryAssessment;
import pegaselenium.utils.Utilities;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

public class ApplicationCompletenessCheck {

	static String title = null;
	static String finalAssessors = null;

	static ArrayList<String> al = null;

	WebDriver driver;
	static InputStream in = null;
	String abnText = null;

	public ApplicationCompletenessCheck(WebDriver driver) {
		this.driver = driver;

	}

	public void clickDropDown() throws InterruptedException {
		driver.findElement(By.xpath(".//*[@id='appview-nav-toggle-one']")).click();

		Thread.sleep(2000);

		driver.findElement(By.xpath(".//*[@data-test-id='201711130507390311962']")).click();

		Thread.sleep(4000);
		//
		Thread.sleep(4000); // uncomment this
		int nframes = driver.findElements(By.tagName("iframe")).size();

		System.out.println("The iframes are" + nframes);

		if (nframes == 1) {
			driver.switchTo().frame(0);
		} else {
			driver.switchTo().frame(1);
		}

		Thread.sleep(2000);
		//driver.switchTo().defaultContent();	
		//driver.findElement(By.name("pyWorkListWidgetHeader_pyDisplayHarness.pxUserDashboard.pySlots(1).pyWidgets(1).pyWidget_9")).click();
		driver.findElement(By.xpath("//span[@id='TMS_TitlesAndCustomerOpsWB']")).click();

		System.out.println("drop down is clicked for selecting Titles and Customer Ops WB");
	}

	public void selectWB() {
		WebElement table = driver.findElement(By.id("bodyTbl_right"));
		table.findElement(By.xpath("//span[@data-test-id='20160923044938017658606']")).click();

		System.out.println("WB is selected");
	}

	public void selectApplicationFromTableOfCases(String appNo) throws InterruptedException, IOException {
		try {
			WebElement titlesListTable = driver.findElement(By.id("bodyTbl_right"));
			Thread.sleep(2000);
			List<WebElement> titlesListRows = titlesListTable.findElements(By.tagName("tr"));
			Thread.sleep(2000);
			WebElement columnValueTitle = null;

			for (int i = 1; i < titlesListRows.size(); i++) {

				System.out.println("titlesListRows size is--->" + titlesListRows.size());
				List<WebElement> columnsForRows = titlesListRows.get(i).findElements(By.tagName("td"));
				Thread.sleep(2000);
				System.out.println("Columns for rows is ---->" + columnsForRows.size());

				for (int j = 0; j < columnsForRows.size(); j++) {

					columnValueTitle = columnsForRows.get(1)
							.findElement(By.xpath(".//*[@data-test-id='201501071438430852311277']"));

					//
					// capturing title
					//

					String filename = "C:\\TMSAutomation\\src\\main\\java\\resources\\config.properties";

					in = new FileInputStream(filename);
					System.out.println("Config file is reached----->");
					Properties prop1 = new Properties();

					prop1.load(in);
					String getTitle = prop1.getProperty("Title");

					System.out.println("The title iam looking for is--->" + getTitle);

					if (columnValueTitle.getText().equalsIgnoreCase(appNo)) // pass
																			// the
																			// value
																			// from
																			// main
																			// pega
																			// screen
																			// After
																			// Title
																			// is
																			// created.
					{
						System.out.println("The columnValueForTitle is ---->" + columnValueTitle.getText());
						columnValueTitle.click();
						Thread.sleep(4000);
						WebDriverWait wait = new WebDriverWait(driver,
								5 /* timeout in seconds */);
						if (wait.until(ExpectedConditions.alertIsPresent()) == null)
							System.out.println("alert was not present");
						else
							System.out.println("alert was present");
						driver.switchTo().alert().dismiss();
					}

				}

			}
		} catch (NoSuchElementException e) {
			System.out.println(e.getMessage());
		} catch (TimeoutException e) {
			System.out.println(e.getMessage());
		} catch (StaleElementReferenceException e) {

			System.out.println(e.getMessage());
		}

		Thread.sleep(10000);
		driver.manage().window().maximize();
		System.out.println("window is maximized");

		Thread.sleep(4000);

		driver.switchTo().defaultContent();

		int framesForReview = driver.findElements(By.tagName("iframe")).size();

		System.out.println("The iframes are" + framesForReview);

		if (framesForReview == 3) {
			driver.switchTo().frame(2);
		} else if (framesForReview == 2) {
			driver.switchTo().frame(1);
		} else {
			driver.switchTo().frame(0);
		}

		// Thread.sleep(4000);
	}

	public void selectApplicationFromTableOfCasesForReassignOperator() throws InterruptedException, IOException {
		try {

			int framesForReview = driver.findElements(By.tagName("iframe")).size();

			System.out.println("The iframes are" + framesForReview);

			if (framesForReview == 3) {
				driver.switchTo().frame(2);
			} else if (framesForReview == 2) {
				driver.switchTo().frame(1);
			} else {
				driver.switchTo().frame(0);
			}

			Thread.sleep(4000);
			WebElement titlesListTable = driver.findElement(By.id("bodyTbl_right"));
			Thread.sleep(2000);
			List<WebElement> titlesListRows = titlesListTable.findElements(By.tagName("tr"));
			Thread.sleep(2000);
			WebElement columnValueTitle = null;

			for (int i = 1; i < titlesListRows.size(); i++) {

				System.out.println("titlesListRows size is--->" + titlesListRows.size());
				List<WebElement> columnsForRows = titlesListRows.get(i).findElements(By.tagName("td"));
				Thread.sleep(2000);
				System.out.println("Columns for rows is ---->" + columnsForRows.size());

				for (int j = 0; j < columnsForRows.size(); j++) {

					columnValueTitle = columnsForRows.get(2)
							.findElement(By.xpath(".//*[@data-test-id='201410081557130000198739']"));

					//
					// capturing title
					//

					String filename = "C:\\TMSAutomation\\src\\main\\java\\resources\\config.properties";

					in = new FileInputStream(filename);
					System.out.println("Config file is reached----->");
					Properties prop1 = new Properties();

					prop1.load(in);
					String getTitle = prop1.getProperty("Title");

					System.out.println("The title iam looking for to reassign is--->" + getTitle);

					if (columnValueTitle.getText().equalsIgnoreCase(getTitle)) // pass
																				// the
																				// value
																				// from
																				// main
																				// pega
																				// screen
																				// After
																				// Title
																				// is
																				// created.
					{
						System.out.println(
								"The columnValueForTitle for reassign is is ---->" + columnValueTitle.getText());
						columnValueTitle.click();
						Thread.sleep(4000);
						WebDriverWait wait = new WebDriverWait(driver,
								5 /* timeout in seconds */);
						if (wait.until(ExpectedConditions.alertIsPresent()) == null)
							System.out.println("alert was not present");
						else
							System.out.println("alert was present");
						driver.switchTo().alert().dismiss();
					}

				}

			}
		} catch (NoSuchElementException e) {
			System.out.println(e.getMessage());
		} catch (TimeoutException e) {
			System.out.println(e.getMessage());
		} catch (StaleElementReferenceException e) {

			System.out.println(e.getMessage());
		}

	}

	public void clickActionsApplicationCompleteness() throws InterruptedException, IOException {

		try {
			WebElement actionsButton = driver.findElement(By.xpath("//button[contains(.,'Actions')]"));

			actionsButton.click();

			Thread.sleep(2000);

			Actions applicationCompleteness = new Actions(driver);
			applicationCompleteness.moveToElement(driver.findElement(By.xpath("//button[contains(.,'Actions')]")))
					.sendKeys(Keys.DOWN).sendKeys(Keys.ENTER).perform();

			System.out.println("Refresh is clicked.............");
		} catch (NoSuchElementException e) {
			System.out.println(e.getMessage());
		} catch (StaleElementReferenceException e) {
			System.out.println(e.getMessage());
		}
	}

	public void enterComments(String comments) {

		driver.findElement(By.id("ApplicationReviewComments")).sendKeys(comments);

	}

	public void clickTASDownloadFile() {
		driver.findElement(By.xpath("//a[@data-test-id='2018110515440601257666']")).click();

	}

	public void clickSubmitApplicationCompletenessCheck() {

		driver.findElement(By.xpath("//button[@data-test-id='201410290229040300882']")).click();
	}

	public void enterTASApplicationID(String tasId) {

		driver.findElement(By.id("LegacyApplicationID")).sendKeys("tasIdv"); // Application
																				// Id
	}

	public void enterDateForRequiredInformationLodged(String reqInfo) {

		driver.findElement(By.id("ApplicationFullySubmitted")).sendKeys(Utilities.getCurrentDate()); // Date
	}

	public void clickSubmitConfirmVerification() {
		driver.findElement(By.xpath("//button[@data-test-id='2014121801251706289770']")).click();
	}

	public void clickActionsRefresh() throws InterruptedException, IOException {

		try {
			WebElement actionsButton = driver.findElement(By.xpath("//button[contains(.,'Actions')]"));

			actionsButton.click();

			Thread.sleep(2000);

			Actions reviewApplication = new Actions(driver);
			reviewApplication.moveToElement(driver.findElement(By.xpath("//button[contains(.,'Actions')]")))
					.sendKeys(Keys.ENTER).perform();

			System.out.println("Refresh is clicked.....Wait........");
		} catch (NoSuchElementException e) {
			System.out.println(e.getMessage());
		} catch (StaleElementReferenceException e) {
			System.out.println(e.getMessage());
		}

		Thread.sleep(6000);

		int framesInAssess = driver.findElements(By.tagName("iframe")).size();

		Thread.sleep(2000);

		System.out.println("The iframes in Assess are" + framesInAssess);
		if (framesInAssess == 3) {
			driver.switchTo().frame(2);
		} else if (framesInAssess == 2) {
			driver.switchTo().frame(1);
		} else {
			driver.switchTo().frame(0);
		}

	}

	public ArrayList<String> captureSubcasesFromAction() throws InterruptedException, IOException {
		try {
			WebElement assignmenttable = driver.findElement(By.id("bodyTbl_gbl"));

			Thread.sleep(4000);

			List<WebElement> assignmenttablerow = assignmenttable.findElements(By.tagName("tr"));
			Thread.sleep(4000);

			System.out.println("Assignment table row size is---->" + assignmenttablerow.size());

			int framesInAssignments = driver.findElements(By.tagName("iframe")).size();

			for (int m = 0; m < assignmenttablerow.size(); m++) {

				List<WebElement> listOfAssessments = assignmenttablerow.get(m)
						.findElements(By.xpath(".//span[@data-test-id='20160908105630099936570']"));
				Thread.sleep(10000);
				al = new ArrayList();
				System.out.println("the size of Assessments are------->" + listOfAssessments.size());
				for (int n = 0; n < listOfAssessments.size(); n++) {

					finalAssessors = listOfAssessments.get(n).getText();
					if (!finalAssessors.equals("Review Assessment")) {
						System.out.println("Finally got the Assessors--------->" + finalAssessors);

						al.add(finalAssessors);

						setAssessors(al);

					}

				}

			}

		} catch (NoSuchElementException e) {
			System.out.println(e.getMessage());
		} catch (StaleElementReferenceException e) {
			System.out.println(e.getMessage());
		}

	
		return al;
	}

	private static void setAssessors(ArrayList<String> al2) throws IOException {

		String Assessors = null;

		for (int i = 0; i < al2.size(); i++) {

			String assessor = al2.get(i).toString();

			int position = assessor.indexOf("(");

			Assessors = assessor.substring(position + 1, assessor.length() - 1);

			System.out.println("The Assessments are ---->" + Assessors);

			// TODO Auto-generated method stub
		}

		String filename = "C:\\TMSAutomation\\src\\main\\java\\resources\\config.properties";

		in = new FileInputStream(filename);
		System.out.println("Config file is reached----->");
		Properties prop = new Properties();
		prop.load(in);
		in.close();

		FileOutputStream out = new FileOutputStream(filename);

		System.out.println("The Assessors after config file are---->" + Assessors);
		if (Assessors.contains("RA-")) {
			prop.setProperty("RAAssessor", Assessors);
		} else if (Assessors.contains("GA-")) {

			prop.setProperty("GAAssessor", Assessors);
		} else if (Assessors.contains("SA-")) {

			prop.setProperty("SAAssessor", Assessors);
		} else if (Assessors.contains("TA-")) {
			prop.setProperty("TAAssessor", Assessors);

		} else if (Assessors.contains("NA-")) {
			prop.setProperty("NAAssessor", Assessors);
		}
		prop.store(out, null);
		System.out.println("Added to config file RAAssessor");

		// TODO Auto-generated method stub

	}

	private static void getAssessors(ArrayList<String> al2) {

		for (int i = 0; i < al2.size(); i++) {

			String assessor = al2.get(i).toString();

			int position = assessor.indexOf("(");

			String Assessors = assessor.substring(position + 1, assessor.length() - 1);

			System.out.println("The Assessments are ---->" + Assessors);
		}

	}

	public void clickActionsReAssign() throws InterruptedException, IOException {

		try {
			WebElement actionsButton = driver.findElement(By.xpath("//button[contains(.,'Actions')]"));

			actionsButton.click();

			Thread.sleep(2000);

			Actions reviewApplication = new Actions(driver);
			reviewApplication.moveToElement(driver.findElement(By.xpath("//button[contains(.,'Actions')]")))
					.sendKeys(Keys.DOWN).sendKeys(Keys.ENTER).perform();

			System.out.println("Reassign is clicked.............");
		} catch (NoSuchElementException e) {
			System.out.println(e.getMessage());
		} catch (StaleElementReferenceException e) {
			System.out.println(e.getMessage());
		}

		Thread.sleep(6000);
	}

	public void assignToOperator() {
		driver.findElement(By.id("pyReassignOperatorName")).click();
		Actions operator = new Actions(driver);
		operator.moveToElement(driver.findElement(By.id("pyReassignOperatorName")))
				.sendKeys("QA1_Titles_Assessment_Manager").sendKeys(Keys.ENTER).perform();
	}

	public void clickReAssignButton() {
		driver.findElement(By.name("pyCaseActionAreaButtons_pyWorkPage_12")).click();
	}

	public void clickLogOff() throws InterruptedException {
		driver.switchTo().defaultContent();

		driver.findElement(By.xpath("//*[@data-test-id='px-opr-image-ctrl']")).click();
		Thread.sleep(4000);
		Actions logoff = new Actions(driver);
		logoff.moveToElement(driver.findElement(By.xpath("//*[@data-test-id='px-opr-image-ctrl']"))).sendKeys(Keys.DOWN)
				.sendKeys(Keys.DOWN).sendKeys(Keys.DOWN).sendKeys(Keys.DOWN).sendKeys(Keys.ENTER).perform();

	}

	public void typeUserNameAssignedOperator() {
		driver.findElement(By.id("txtUserID")).sendKeys("QA1_Titles_Assessment_Manager");
	}

	public void typePasswordAssignedOperator() {
		driver.findElement(By.id("txtPassword")).sendKeys("Test@1233");
	}

	public void clickMyWorkFromMenu() throws InterruptedException {

		driver.findElement(By.xpath(".//*[@id='appview-nav-toggle-one']")).click();
		Thread.sleep(2000);
		WebElement menu = driver.findElement(By.xpath(".//span[@class='menu-item-title']"));
		menu.click();

		Actions actions = new Actions(driver);
		actions.moveToElement(menu).perform();
		driver.findElement(By.linkText("My Work")).click();

	}

}
