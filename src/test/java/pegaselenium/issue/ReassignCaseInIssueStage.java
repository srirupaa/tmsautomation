package pegaselenium.issue;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Properties;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

public class ReassignCaseInIssueStage {

	String assessor = null;
	String Assessors = null;
	int position =0;
	static WebDriver driver =null;
	static FileInputStream in = null;
	static Properties prop = null;



		 
		 //dev
//		driver.get("https://nswpe-dev1.pegacloud.io/prweb/beEBp4uRVTogorRwSwWqbOtn9IL2fwdI*/!STANDARD");
//		driver.manage().window().maximize();
//		driver.findElement(By.id("txtUserID")).sendKeys("TMS_TitlesAssessmentUser");
//		Thread.sleep(2000);
//		driver.findElement(By.id("txtPassword")).sendKeys("Dev@1234");
//		Thread.sleep(2000);
//		driver.findElement(By.id("sub")).click();
//		
		//SIT
			
				
				public ReassignCaseInIssueStage(WebDriver driver){
					this.driver=driver;
					
					
				}
				public void typeUserName(){
					driver.findElement(By.id("txtUserID")).sendKeys("QA1_Titles_Assessment_User");
				}
				public void typePassword(){
					driver.findElement(By.id("txtPassword")).sendKeys("Test@1233");
				}
				
				public void clickSubmit(){
					
					driver.findElement(By.id("sub")).click();
					
				}
		
		
		
		public void clickDropDown() throws InterruptedException{
		
		driver.manage().window().maximize();
		int framesForReview = driver.findElements(By.tagName("iframe")).size();
		
		System.out.println("The iframes are"+framesForReview);
		
	if(framesForReview==3){
		driver.switchTo().frame(2);
		}else
			if(framesForReview==2)
		{
			driver.switchTo().frame(1);
		}else{
			driver.switchTo().frame(0);
		}
		try{
			
			driver.manage().window().maximize();
		
			
			driver.findElement(By.xpath("//i[contains(@class,'cursordefault icons pi pi-caret-solid-down')]")).click();
			
			
			
			
			System.out.println("dropdown is clicked");
			
		
		}catch(NoSuchElementException e){
			System.out.println(e.getMessage());
		}
		Thread.sleep(6000);
		
		}
		
		
		public void selectWB() throws InterruptedException{
		try{
	WebElement wbTable=	driver.findElement(By.id("gridLayoutTable"));
	
	List<WebElement> wbTableList= wbTable.findElements(By.tagName("tr"));
	
	Thread.sleep(4000);
	
	for(int j =0;j<wbTableList.size();j++){
		
		
	List<WebElement> selectWbTRC  = wbTableList.get(j).findElements(By.tagName("td"));
	Thread.sleep(4000);
	for(int k =0;k<selectWbTRC.size();k++){
		
	
		
		selectWbTRC.get(k).findElement(By.xpath("//span[contains(text(),'Titles Assessment WB')]")).click();
	}
	
	}
	}
		catch(StaleElementReferenceException e){
			
			System.out.println(e.getMessage());
	
	
	
	
	
	}
	
	System.out.println("Wb is selected");
		
		
		Thread.sleep(4000);
		
		
		
		int nframes = driver.findElements(By.tagName("iframe")).size();
		
		System.out.println("The iframes are"+nframes);
		}
		
		public void selectApplicationForReassignInIssueStage() throws InterruptedException, IOException{
		
		
		
	WebElement nopdTable=	driver.findElement(By.id("bodyTbl_right")); // //bodyTbl_right
	
	Thread.sleep(2000);
	List<WebElement> nopdTableRows = nopdTable.findElements(By.tagName("tr"));
	Thread.sleep(2000);
	
	System.out.println("the Size of Rowsin the table ---->"+nopdTableRows.size());
	
	

	
	
String filename = "C:\\TMSAutomation\\src\\main\\java\\resources\\config.properties";
	
	in = new FileInputStream(filename);
	System.out.println("Config file is reached----->");
	prop = new Properties();
	
	prop.load(in);
String sendNOPDToCustomerAssessor=	prop.getProperty("Title"); // getting Title from config file for checking in the list
	
	
	System.out.println("The title to send NOPD to customer is------->"+sendNOPDToCustomerAssessor);
	
	
	for(int i = 0;i<nopdTableRows.size();i++){
		
		List<WebElement> nopdTitleColumns = nopdTableRows.get(i).findElements(By.tagName("td"));
		
		for(int j = 0;j<nopdTitleColumns.size();j++)
		{
		

			System.out.println("rownum--->"+nopdTitleColumns.size());
			if(nopdTitleColumns.get(1).getText().equalsIgnoreCase(sendNOPDToCustomerAssessor)){
				System.out.println("successfully inside nopdtocustomerAssessor");
				
				nopdTitleColumns.get(1).click();
			}
	
		}
	
		
	}	
	
	Thread.sleep(10000);
	


	driver.manage().window().maximize();
	System.out.println("window is maximized");
	
	Thread.sleep(4000);
	
		
	
	
	driver.switchTo().defaultContent();
int framesForReview = driver.findElements(By.tagName("iframe")).size();
	
	System.out.println("The iframes are"+framesForReview);
	
if(framesForReview==3){
	driver.switchTo().frame(2);
	}else
		if(framesForReview==2)
	{
		driver.switchTo().frame(1);
	}else{
		driver.switchTo().frame(0);
	}

		}

		public void clickActionsReAssign() throws InterruptedException, IOException{
			
			try{
				WebElement actionsButton = driver.findElement(By.xpath("//button[contains(.,'Actions')]"));


			actionsButton.click();

			Thread.sleep(2000);

			Actions reviewApplication= new Actions(driver);
			reviewApplication.moveToElement(driver.findElement(By.xpath("//button[contains(.,'Actions')]"))).sendKeys(Keys.DOWN).sendKeys(Keys.DOWN).sendKeys(Keys.ENTER).perform();
			System.out.println("Reassign is clicked.............");
			}catch(NoSuchElementException e){
				System.out.println(e.getMessage());
			}catch(StaleElementReferenceException e){
				System.out.println(e.getMessage());
			}

			Thread.sleep(6000);
}
		
		public void assignToOperator(){
			driver.findElement(By.id("pyReassignOperatorName")).click();
			Actions operator = new Actions(driver);
			operator.moveToElement(driver.findElement(By.id("pyReassignOperatorName"))).sendKeys("QA1_Titles_Assessment_Manager").sendKeys(Keys.ENTER).perform(); 
		}
		
		public void clickReAssignButton(){
			driver.findElement(By.name("pyCaseActionAreaButtons_pyWorkPage_12")).click();
		}
		public void clickLogOff() throws InterruptedException{
			driver.switchTo().defaultContent();
					
					driver.findElement(By.xpath("//*[@data-test-id='px-opr-image-ctrl']")).click();
					Thread.sleep(4000);
					Actions logoff = new Actions(driver);
					logoff.moveToElement(driver.findElement(By.xpath("//*[@data-test-id='px-opr-image-ctrl']"))).sendKeys(Keys.DOWN).sendKeys(Keys.DOWN).sendKeys(Keys.DOWN).sendKeys(Keys.DOWN).sendKeys(Keys.ENTER).perform();
				
				}
		public void typeUserNameAssignedOperator(){
			driver.findElement(By.id("txtUserID")).sendKeys("QA1_Titles_Assessment_Manager");
		}
		public void typePasswordAssignedOperator(){
			driver.findElement(By.id("txtPassword")).sendKeys("Test@1233");
		}
		
		public void clickMyWorkFromMenu() throws InterruptedException{
			
			driver.findElement(By.xpath(".//*[@id='appview-nav-toggle-one']")).click();
			Thread.sleep(2000);
			WebElement menu = driver.findElement(By.xpath(".//span[@class='menu-item-title']"));
					menu.click();
			
				Thread.sleep(2000);	
			Actions actions= new Actions(driver);
			actions.moveToElement(menu).perform();
			driver.findElement(By.linkText("My Work")).click();

		}
		
		public void selectApplicationFromTableOfCasesForReassignOperator() throws InterruptedException, IOException{
			try{
				
				int framesForReview = driver.findElements(By.tagName("iframe")).size();
				
				System.out.println("The iframes are"+framesForReview);
				
			if(framesForReview==3){
				driver.switchTo().frame(2);
				}else
					if(framesForReview==2)
				{
					driver.switchTo().frame(1);
				}else{
					driver.switchTo().frame(0);
				}


				
				Thread.sleep(4000);
			WebElement titlesListTable = driver.findElement(By.id("bodyTbl_right"));
			Thread.sleep(2000);
			List<WebElement> titlesListRows = titlesListTable.findElements(By.tagName("tr"));
			Thread.sleep(2000);
			WebElement columnValueTitle = null;
			
			for(int i = 1; i<titlesListRows.size();i++){
				
			
				System.out.println("titlesListRows size is--->"+titlesListRows.size());
		List<WebElement> columnsForRows =	titlesListRows.get(i).findElements(By.tagName("td"));
		Thread.sleep(2000);
		System.out.println("Columns for rows is ---->"+columnsForRows.size());

		for(int j = 0;j<columnsForRows.size();j++){
			

			
		


			columnValueTitle = 	columnsForRows.get(2).findElement(By.xpath(".//*[@data-test-id='201410081557130000198739']"));
			
			
			//
			//capturing title
			//
			
		String filename = "C:\\TMSAutomation\\src\\main\\java\\resources\\config.properties";
			
			in = new FileInputStream(filename);
			System.out.println("Config file is reached----->");
			Properties prop1 = new Properties();
			
			prop1.load(in);
		String getTitle = prop1.getProperty("Title");

		System.out.println("The title iam looking for to reassign is--->"+getTitle);
			
			
			if(columnValueTitle.getText().equalsIgnoreCase(getTitle)) // pass the value from main pega screen After Title is created.
			{
				System.out.println("The columnValueForTitle for reassign is is ---->"+columnValueTitle.getText());
		columnValueTitle.click();
		Thread.sleep(4000);
		WebDriverWait wait = new WebDriverWait(driver, 5 /*timeout in seconds*/);
		if(wait.until(ExpectedConditions.alertIsPresent())==null)
		    System.out.println("alert was not present");
		else
		    System.out.println("alert was present");
		driver.switchTo().alert().dismiss();
			}

			}

		}
			}catch(NoSuchElementException e){
				System.out.println(e.getMessage());
			}
			catch(TimeoutException e){
				System.out.println(e.getMessage());
			}catch(StaleElementReferenceException e){
				
				System.out.println(e.getMessage());
			}
			
		
		}
	


}
