package pegaselenium.issue;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.Select;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import pegaselenium.apply.CreateApplication;
import pegaselenium.login.LoginPage;
import pegaselenium.utils.DPETestBase;
import pegaselenium.utils.ExcelUtils;

public class SendNOPDToCustomer extends DPETestBase {

	String assessor = null;
	String Assessors = null;
	int position = 0;
	static WebDriver driver = null;
	static FileInputStream in = null;
	static Properties prop = null;

	LoginPage login;
	CreateApplication createApplication;

	@BeforeTest
	public void setUP() {

		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
		driver.get(env.getBaseURL());

		login = new LoginPage(driver);
		createApplication = new CreateApplication(driver);

	}

	@DataProvider(name = "NOPDData")
	public Object[][] createData() throws Exception {
		ExcelUtils xUtil = new ExcelUtils("testdata\\TitleData.xlsx");
		Object[][] retObjArr = xUtil.getXLDataByPOI("Title Data");
		return (retObjArr);
	}

	@Test(dataProvider = "NOPDData")
	public void sendNOPDToCustomer(String title, String comments, String reviewDate) throws InterruptedException, IOException {
		// TODO Auto-generated method stub

		login.loginAsTitlesAssessmentUser();

		int framesForReview = driver.findElements(By.tagName("iframe")).size();

		System.out.println("The iframes are" + framesForReview);

		if (framesForReview == 3) {
			driver.switchTo().frame(2);
		} else if (framesForReview == 2) {
			driver.switchTo().frame(1);
		} else {
			driver.switchTo().frame(0);
		}
		try {

			// select drop down
			driver.findElement(By.xpath("//i[contains(@class,'cursordefault icons pi pi-caret-solid-down')]")).click();

			System.out.println("dropdown is clicked");

		} catch (NoSuchElementException e) {
			System.out.println(e.getMessage());
		}
		Thread.sleep(6000);

		// select Titles Assessment WB

		try {
			WebElement wbTable = driver.findElement(By.id("gridLayoutTable"));

			List<WebElement> wbTableList = wbTable.findElements(By.tagName("tr"));

			Thread.sleep(4000);

			for (int j = 0; j < wbTableList.size(); j++) {

				List<WebElement> selectWbTRC = wbTableList.get(j).findElements(By.tagName("td"));
				Thread.sleep(4000);
				for (int k = 0; k < selectWbTRC.size(); k++) {

					selectWbTRC.get(k).findElement(By.xpath("//span[contains(text(),'Titles Assessment WB')]")).click();
				}

			}
		} catch (StaleElementReferenceException e) {

			System.out.println(e.getMessage());

		}

		System.out.println("Wb is selected");

		Thread.sleep(4000);
		int nframes = driver.findElements(By.tagName("iframe")).size();

		System.out.println("The iframes are" + nframes);

		WebElement nopdTable = driver.findElement(By.id("bodyTbl_right"));

		Thread.sleep(2000);
		List<WebElement> nopdTableRows = nopdTable.findElements(By.tagName("tr"));
		Thread.sleep(2000);

		System.out.println("the Size of Rowsin the table ---->" + nopdTableRows.size());

		// select Title that was dynamically created as part of Create
		// Application

		String filename = "C:\\TMSAutomation\\src\\main\\java\\resources\\config.properties";

		in = new FileInputStream(filename);
		System.out.println("Config file is reached----->");
		prop = new Properties();

		prop.load(in);
		String sendNOPDToCustomerAssessor = prop.getProperty("Title"); // getting
																		// Title
																		// from
																		// config
																		// file
																		// for
																		// checking
																		// in
																		// the
																		// list

		System.out.println("The title to send NOPD to customer is------->" + sendNOPDToCustomerAssessor);

		for (int i = 0; i < nopdTableRows.size(); i++) {

			List<WebElement> nopdTitleColumns = nopdTableRows.get(i).findElements(By.tagName("td"));

			for (int j = 0; j < nopdTitleColumns.size(); j++) {

				// System.out.println("rownum--->"+nopdTitleColumns.size());
				if (nopdTitleColumns.get(1).getText().equalsIgnoreCase(sendNOPDToCustomerAssessor)) {
					System.out.println("successfully inside nopdtocustomerAssessor");

					nopdTitleColumns.get(1).click();
				}

			}

		}

		Thread.sleep(10000);

		driver.manage().window().maximize();
		System.out.println("window is maximized");

		Thread.sleep(4000);

		driver.switchTo().defaultContent();

		int framesfortextarea = driver.findElements(By.tagName("iframe")).size();

		System.out.println("The iframes for area are" + framesfortextarea);

		if (framesfortextarea == 3) {
			driver.switchTo().frame(2);
		} else if (framesfortextarea == 2) {
			driver.switchTo().frame(1);
		} else {
			driver.switchTo().frame(0);
		}

		Thread.sleep(4000);

		// selecting Yes for Is NOPD Issued To Customer
		Select nopdSelect = new Select(driver.findElement(By.id("IsNOPDIssuedToCustomer")));
		Thread.sleep(2000);

		nopdSelect.selectByValue("Yes");

		Thread.sleep(2000);
		// select Date
		WebElement nopdProposedDeterminationDate = driver
				.findElement(By.xpath("//*[@data-test-id='2018110318454903442162-DatePicker']"));
		nopdProposedDeterminationDate.click();
		Thread.sleep(2000);

		driver.findElement(By.id("todayLink")).click();
		Thread.sleep(2000);
		// enter comments for determination notes
		driver.findElement(By.id("DeterminationNotes")).sendKeys("Sending NOPD to Customer");
		Thread.sleep(2000);
		// select Grant

		Select chooseDetermination = new Select(driver.findElement(By.id("pyGotoStage")));

		chooseDetermination.selectByVisibleText("Grant");
		Thread.sleep(2000);
		// enter TASTitle ID
		driver.findElement(By.id("TASTitleID")).sendKeys("TAS1234");
		Thread.sleep(2000);

		// enter NOPD Audit Notes
		driver.findElement(By.id("pyAuditNote")).sendKeys("Sending NOPD Audit to Customer");

		Thread.sleep(3000);

		// Click Submit
		driver.findElement(By.xpath("//button[contains(.,'Submit')]")).click();
		// End of Review

		Thread.sleep(6000);

		System.out.println("NOPD is completed hurrah!!!!!!!");

		Thread.sleep(6000);

		try {
			WebElement actionsButton = driver.findElement(By.xpath("//button[contains(.,'Actions')]"));

			actionsButton.click();

			Thread.sleep(2000);

			Actions reviewApplication = new Actions(driver);
			reviewApplication.moveToElement(driver.findElement(By.xpath("//button[contains(.,'Actions')]")))
					.sendKeys(Keys.ENTER).perform();
			System.out.println("Refresh is clicked.............");
		} catch (NoSuchElementException e) {
			System.out.println(e.getMessage());
		} catch (StaleElementReferenceException e) {
			System.out.println(e.getMessage());
		}

		Thread.sleep(6000);

		int framesInAssess = driver.findElements(By.tagName("iframe")).size();

		Thread.sleep(2000);

		System.out.println("The iframes in Assess are" + framesInAssess);
		if (framesInAssess == 3) {
			driver.switchTo().frame(2);
		} else if (framesInAssess == 2) {
			driver.switchTo().frame(1);
		} else {
			driver.switchTo().frame(0);
		}

		Thread.sleep(6000);

	}

}
