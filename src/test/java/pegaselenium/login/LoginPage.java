package pegaselenium.login;

import java.io.IOException;

import org.openqa.selenium.By;
import org.openqa.selenium.ElementNotVisibleException;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;

import pegaselenium.utils.DriverPage;
import pegaselenium.utils.ExcelUtils;
import pegaselenium.utils.Utilities;

public class LoginPage extends DriverPage{

	public LoginPage(WebDriver driver) {
		super(driver);

	}

	public void typeUserName(String username) {
		driver.findElement(By.id("txtUserID")).sendKeys(username);
	}

	public void typePassword(String pwd) {
		driver.findElement(By.id("txtPassword")).sendKeys(pwd);
	}

	public void clickSubmit() {

		driver.findElement(By.id("sub")).click();

	}
	
		
	public void loginUsingCustomerOpsUser() throws IOException{
		ExcelUtils xUtils = new ExcelUtils("testdata\\LoginCredentials.xlsx");
		String[] customerOpsUser=xUtils.getXLDataLatestByRow("Login",1);
		typeUserName(customerOpsUser[0]);
		typePassword(customerOpsUser[1]);
		clickSubmit();
	}
	
	public Boolean isUserLoggedIn(){
		boolean loggedIn=false;
		try{
			loggedIn= driver.findElement(By.xpath("//div[@data-test-id='201810261609440701676']")).isDisplayed();
		}
		catch(NoSuchElementException e){
			
		}
		catch(ElementNotVisibleException e){
			
		}
		return loggedIn;
	}
	
	public void loginUsingCustomerOpsManager() throws IOException{
		ExcelUtils xUtils = new ExcelUtils("testdata\\LoginCredentials.xlsx");
		String[] customerOpsUser=xUtils.getXLDataLatestByRow("Login",2);
		typeUserName(customerOpsUser[0]);
		typePassword(customerOpsUser[1]);
		clickSubmit();
	}
	
	public void loginAsGeoAssessmentUser() throws IOException{
		ExcelUtils xUtils = new ExcelUtils("testdata\\LoginCredentials.xlsx");
		String[] customerOpsUser=xUtils.getXLDataLatestByRow("Login",5);
		typeUserName(customerOpsUser[0]);
		typePassword(customerOpsUser[1]);
		clickSubmit();
	}
	
	public void loginAsTitlesAssessmentUser() throws IOException{
		ExcelUtils xUtils = new ExcelUtils("testdata\\LoginCredentials.xlsx");
		String[] customerOpsUser=xUtils.getXLDataLatestByRow("Login",3);
		typeUserName(customerOpsUser[0]);
		typePassword(customerOpsUser[1]);
		clickSubmit();
	}
	public void loginAsRRComplianceUser() throws IOException{
		ExcelUtils xUtils = new ExcelUtils("testdata\\LoginCredentials.xlsx");
		String[] customerOpsUser=xUtils.getXLDataLatestByRow("Login",4);
		typeUserName(customerOpsUser[0]);
		typePassword(customerOpsUser[1]);
		clickSubmit();
	}
	public void loginAsSpatialServicesUser() throws IOException{
		ExcelUtils xUtils = new ExcelUtils("testdata\\LoginCredentials.xlsx");
		String[] customerOpsUser=xUtils.getXLDataLatestByRow("Login",6);
		typeUserName(customerOpsUser[0]);
		typePassword(customerOpsUser[1]);
		clickSubmit();
	}
	public void loginAsRegandOpsUser() throws IOException{
		ExcelUtils xUtils = new ExcelUtils("testdata\\LoginCredentials.xlsx");
		String[] customerOpsUser=xUtils.getXLDataLatestByRow("Login",7);
		typeUserName(customerOpsUser[0]);
		typePassword(customerOpsUser[1]);
		clickSubmit();
	}
	
	public void loginTitlesAssessmentManager() throws IOException{
		ExcelUtils xUtils = new ExcelUtils("testdata\\LoginCredentials.xlsx");
		String[] customerOpsUser=xUtils.getXLDataLatestByRow("Login",11);
		typeUserName(customerOpsUser[0]);
		typePassword(customerOpsUser[1]);
		clickSubmit();
	}
		
	public void loginTMSManager() throws IOException{
		ExcelUtils xUtils = new ExcelUtils("testdata\\LoginCredentials.xlsx");
		String[] customerOpsUser=xUtils.getXLDataLatestByRow("Login",10);
		typeUserName(customerOpsUser[0]);
		typePassword(customerOpsUser[1]);
		clickSubmit();
	}
	
	public void loginTitlesDecisioningUser() throws IOException{
		ExcelUtils xUtils = new ExcelUtils("testdata\\LoginCredentials.xlsx");
		String[] customerOpsUser=xUtils.getXLDataLatestByRow("Login",9);
		typeUserName(customerOpsUser[0]);
		typePassword(customerOpsUser[1]);
		clickSubmit();
	}
	
	public void loginAsTRCRecommendationUser() throws IOException{
		ExcelUtils xUtils = new ExcelUtils("testdata\\LoginCredentials.xlsx");
		String[] customerOpsUser=xUtils.getXLDataLatestByRow("Login",8);
		typeUserName(customerOpsUser[0]);
		typePassword(customerOpsUser[1]);
		clickSubmit();
	}

	public boolean isGeoUserLoggedIn() {
		// TODO Auto-generated method stub
		boolean loggedIn=false;
		try{
			loggedIn= driver.findElement(By.xpath("//i[@title='TMS_GeoAssessmentUser']")).isDisplayed();
		}
		catch(NoSuchElementException e){
			System.out.println(e.getMessage());
		}
		catch(ElementNotVisibleException e){
			System.out.println(e.getMessage());
		}
		return loggedIn;
		
	}
	
	public boolean isCustomerManagerLoggedIn() {
		// TODO Auto-generated method stub
		boolean loggedIn=false;
		try{
			loggedIn= driver.findElement(By.xpath("//i[@title='TMS_TitlesandCustomerOpsMgrs']")).isDisplayed();
		}
		catch(NoSuchElementException e){
			System.out.println(e.getMessage());
		}
		catch(ElementNotVisibleException e){
			System.out.println(e.getMessage());
		}
		return loggedIn;
		
	}
	public boolean isSpatialUserLoggedIn() {
		// TODO Auto-generated method stub
		boolean loggedIn=false;
		try{
			loggedIn= driver.findElement(By.xpath("//i[@title='TMS_SpatialServicesUser']")).isDisplayed();
		}
		catch(NoSuchElementException e){
			
		}
		catch(ElementNotVisibleException e){
			
		}
		return loggedIn;
		
	}
	
	public boolean isNativeTitleUserLoggedIn() {
		// TODO Auto-generated method stub
		boolean loggedIn=false;
		try{
			loggedIn= driver.findElement(By.xpath("//i[@title='TMS_RegandOpsAdviceUser']")).isDisplayed();
		}
		catch(NoSuchElementException e){
			
		}
		catch(ElementNotVisibleException e){
			
		}
		return loggedIn;
		
	}
	public boolean isTitlesUserLoggedIn() {
		// TODO Auto-generated method stub
		boolean loggedIn=false;
		try{
			loggedIn= driver.findElement(By.xpath("//i[@title='TMS_TitlesAssessmentUser']")).isDisplayed();
		}
		catch(NoSuchElementException e){
			
		}
		catch(ElementNotVisibleException e){
			
		}
		return loggedIn;
		
	}
	
	public boolean isRRComplianceUserLoggedIn() {
		// TODO Auto-generated method stub
		boolean loggedIn=false;
		try{
			loggedIn= driver.findElement(By.xpath("//i[@title='TMS_RRComplianceAssessUser']")).isDisplayed();
		}
		catch(NoSuchElementException e){
			
		}
		catch(ElementNotVisibleException e){
			
		}
		return loggedIn;
		
	}

	
}
