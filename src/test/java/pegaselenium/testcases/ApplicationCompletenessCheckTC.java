package pegaselenium.testcases;

import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.concurrent.TimeUnit;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import pegaselenium.apply.CreateApplication;
import pegaselenium.check.ApplicationCompletenessCheck;
import pegaselenium.login.LoginPage;
import pegaselenium.utils.DPETestBase;
import pegaselenium.utils.ExcelUtils;
import screenShot.ScreenShot;



public class ApplicationCompletenessCheckTC extends DPETestBase {
	
	ApplicationCompletenessCheck check ;
	CreateApplication createApplication;
	ScreenShot screenshot;
	LoginPage login;
	@BeforeTest
	public void setUP(){
		

		login = new LoginPage(driver);
		createApplication = new CreateApplication(driver);
		check = new ApplicationCompletenessCheck(driver);
	}
	@DataProvider(name = "CheckData")
	public Object[][] createData() throws Exception {
		ExcelUtils xUtil = new ExcelUtils("testdata\\CheckData.xlsx");
		Object[][] retObjArr = xUtil.getXLDataByPOI("Check Data");
		return (retObjArr);
	}

	// Application from Menu
	@Test(dataProvider = "CheckData")
	public void applicationCompletenessCheck(String applicationNo, String comments, String tasID, String reqInfo) throws InterruptedException, IOException{
		
		driver.switchTo().defaultContent();	
		if(!login.isCustomerManagerLoggedIn())
			login.loginUsingCustomerOpsManager();
		login.navigateToDashboard();
		check.clickDropDown();
		Thread.sleep(4000);
		//check.selectWB();
		//Thread.sleep(2000);
		check.selectApplicationFromTableOfCases(applicationNo);
		Thread.sleep(4000);
//		check.clickActionsApplicationCompleteness();
//		Thread.sleep(4000);
		check.enterComments(comments);
		Thread.sleep(2000);
		check.clickTASDownloadFile();
		Thread.sleep(2000);
		
		//enterTASApplicationID
		
		check.clickSubmitApplicationCompletenessCheck();
		check.enterTASApplicationID(tasID);
		check.enterDateForRequiredInformationLodged(reqInfo);
		check.clickSubmitConfirmVerification();
		Thread.sleep(4000);
		
		//clickActionAndCaptureSubcasesFromAction
		
		check.clickActionsRefresh();
		Thread.sleep(6000);
		ArrayList<String> list = check.captureSubcasesFromAction();
		
		try
		{
		    String filename= "testdata\\assessors.txt";
		    FileWriter fw = new FileWriter(filename,true); //the true will append the new data
		    fw.write("\n"
		    		+ list
		    		+ "\n");//appends the string to the file
		    fw.close();
		}
		catch(IOException ioe)
		{
		    System.err.println("IOException: " + ioe.getMessage());
		}
	}
	
}
