package pegaselenium.testcases;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import pegaselenium.apply.CreateApplication;
import pegaselenium.login.LoginPage;

public class CreateApplicationForMinistersConsentMineralGroup11TC {
CreateApplication createApplication;
LoginPage login;
	@BeforeTest
	public void setUP(){
	
		
		login = new LoginPage(driver);
		createApplication = new CreateApplication(driver);
	}
	
	// Application from Menu
	@Test(priority=1)
	public void clickApplicationFromMenu() throws InterruptedException, IOException{
		
		login.loginUsingCustomerOpsUser();
		login.clickSubmit();
	  createApplication.clickApplicationFromMenu();
	}
	
	// New Harness screen
	@Test(priority=2)
	public void newHarnessScreen() throws InterruptedException{
	  createApplication.selectApplicationType();
	  createApplication.selectAuthorityType();
	  createApplication.selectTerm();
	  createApplication.clickDone();
	}
	
	
	  //Application Details screen with mineralgroup11 and ministers consent and date selected
	@Test(priority=3)
	public void applicationDetails() throws InterruptedException{
	  createApplication.selectLicence();
	  createApplication.selectMineralGroup11();
	  Thread.sleep(10000);
	  createApplication.selectMinisterConsent();
	  Thread.sleep(4000);
	  createApplication.enterDateForMinistersConsent();
	  Thread.sleep(4000);
	  createApplication.pressContinue();
		}
	
	// Party Details screen
	@Test(priority=4)
	public void partyDetails() throws InterruptedException{
	  createApplication.selectPartyFromTextBox(1);
	  Thread.sleep(4000);
	  createApplication.clickAddParty();
	  Thread.sleep(4000);
	  createApplication.selectPartyFromTextBox(2);
	  Thread.sleep(4000);
	  createApplication.clickAddParty();
	  Thread.sleep(4000);
	  createApplication.selectRole1();
	  Thread.sleep(4000);
	  createApplication.selectRole2();
	  Thread.sleep(4000);
	  createApplication.pressContinue();
	  Thread.sleep(4000);
	  
	}
	  
	// Define Area screen
	@Test(priority=5)
	public void defineArea() throws InterruptedException{
		createApplication.clickAddBlocksAndUnits();
		createApplication.pressContinue();
		}
	  
	// Provide Documents
	
	@Test(priority=6)
	public void provideDocuments() throws InterruptedException{
		
		createApplication.clickAddAttachment();
		createApplication.selectFileCategory();
		Thread.sleep(2000);
		createApplication.clickAttachInAttachFile();
		Thread.sleep(2000);
		createApplication.pressContinue();
		
		}
	
	// Provide Declaration
	
	@Test(priority=7)
	public void provideDeclaration() throws InterruptedException, IOException{
		
		createApplication.selectCheckBoxDeclaration();
		Thread.sleep(2000);
		  createApplication.clickFinish();
		  Thread.sleep(4000);
	}
	
	
	@Test(priority=8)
	public void captureApplicationNumber() throws IOException{
		
		createApplication.CaptureApplicationNumber();
	}
	    

}
