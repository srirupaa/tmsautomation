package pegaselenium.testcases;

import java.io.FileWriter;
import java.io.IOException;
import java.util.concurrent.TimeUnit;

import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import pegaselenium.apply.ApplicationDetails;
import pegaselenium.apply.CreateApplication;
import pegaselenium.apply.DefineArea;
import pegaselenium.apply.PartyDetails;
import pegaselenium.apply.ProvideDeclaration;
import pegaselenium.apply.ProvideDocuments;
import pegaselenium.login.LoginPage;
import pegaselenium.utils.DPETestBase;
import pegaselenium.utils.ExcelUtils;
import pegaselenium.utils.Utilities;
import screenShot.ScreenShot;

public class CreateApplicationTC extends DPETestBase {

	CreateApplication createApplication;
	ScreenShot screenshot;
	LoginPage login;
	String str = "";
	int i =0;
	

	@BeforeTest
	public void setUP() {

		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
		driver.get(env.getBaseURL());

		login = new LoginPage(driver);
		createApplication = new CreateApplication(driver);

	}

	@DataProvider(name = "ApplyData")
	public Object[][] createData() throws Exception {
		ExcelUtils xUtil = new ExcelUtils("testdata\\ApplyData.xlsx");
		Object[][] retObjArr = xUtil.getXLDataByPOI("Apply Data");
		return (retObjArr);
	}

	// Application from Menu
	@Test(dataProvider = "ApplyData")
	public void createApplication(String applicationType, String authorityType, String term, String licenceType, String nativeTitle,
			String minGrp, String appReceivedDate, String consentFlag, String mineralAllocFlag, String party1, String role1, String party2, String role2, String mapSheetName, String blocks, String units, String fileName,
			String fileCate) throws Exception {

		if(!login.isUserLoggedIn())
			login.loginUsingCustomerOpsUser();
		//System.out.println("Consent date: "+consent);
		Utilities.captureSnapshot(driver);
		createApplication.clickApplicationFromMenu();
		createApplication.selectApplicationType(applicationType);
		createApplication.selectAuthorityType(authorityType);
		createApplication.selectTerm(term);
		Utilities.captureSnapshot(driver);
		createApplication.clickDone();
		ApplicationDetails appDetails = new ApplicationDetails(driver);
		appDetails.selectLicence(licenceType,nativeTitle);
	
		Thread.sleep(2000);
		if (minGrp.contains("1")){
			appDetails.selectMineralGroup1(minGrp);
			appDetails.selectMineralAllocationArea(mineralAllocFlag);
		}
			
		else if (minGrp.contains("8")){
			appDetails.selectMineralGroup8();
			appDetails.selectMineralAllocationArea(mineralAllocFlag);
		}
		else if (minGrp.contains("11")){
			appDetails.selectMineralGroup11();
			appDetails.selectMinisterConsent(consentFlag);
		}
			
		else
			appDetails.selectMineralGroup1(minGrp);
		Thread.sleep(2000);

		appDetails.setEffectiveDate();
			Utilities.captureSnapshot(driver);
		createApplication.pressContinue();
		Thread.sleep(4000);
		PartyDetails partyDetails = new PartyDetails(driver);
		partyDetails.selectPartyFromTextBox(0,party1);
		Thread.sleep(4000);
		partyDetails.clickAddParty();
		Thread.sleep(4000);
		partyDetails.selectPartyFromTextBox(1, party2);
		Thread.sleep(4000);
		partyDetails.clickAddParty();
		Thread.sleep(4000);
		// createApplication.selectPartyFromTextBox(2);
		// Thread.sleep(4000);
		// createApplication.clickAddParty();
		// Thread.sleep(4000);
		partyDetails.selectRole1(role1);
		Thread.sleep(4000);
		partyDetails.selectRole2(role2);
		Thread.sleep(4000);
		// createApplication.selectRole3();
		// Thread.sleep(4000);
		Utilities.captureSnapshot(driver);
		createApplication.pressContinue();
		Thread.sleep(4000);
		DefineArea definearea = new DefineArea(driver);
		definearea.clickAddBlocksAndUnits(mapSheetName, blocks, units);
		Utilities.captureSnapshot(driver);
		createApplication.pressContinue();
		// createApplication.pressContinue(); - Rehab screenv
		ProvideDocuments provideDocs = new ProvideDocuments(driver);
		provideDocs.clickAddAttachment(fileName);
		provideDocs.selectFileCategory(fileCate);
		Thread.sleep(2000);
		provideDocs.clickAttachInAttachFile();
		Thread.sleep(2000);
		Utilities.captureSnapshot(driver);
		createApplication.pressContinue();
		ProvideDeclaration provideDec = new ProvideDeclaration(driver);
		provideDec.selectCheckBoxDeclaration();
		Thread.sleep(2000);
		Utilities.captureSnapshot(driver);
		createApplication.pressFinish();
		Thread.sleep(4000);
		str = createApplication.CaptureApplicationNumber();
		
		try
		{
		    String filename= "testdata\\results.txt";
		    FileWriter fw = new FileWriter(filename,true); //the true will append the new data
		    fw.write("\n"
		    		+ str
		    		+ "\n");//appends the string to the file
		    fw.close();
		}
		catch(IOException ioe)
		{
		    System.err.println("IOException: " + ioe.getMessage());
		}
		Utilities.captureSnapshot(driver);
		
		
	}
	
	@AfterMethod(alwaysRun=true)
	public void afterMethod(){
		driver.get(env.getBaseURL());
	}
	

}
