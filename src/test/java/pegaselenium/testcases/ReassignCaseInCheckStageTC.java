package pegaselenium.testcases;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import pegaselenium.check.ApplicationCompletenessCheck;
import pegaselenium.login.LoginPage;
import pegaselenium.utils.DPETestBase;

// First Create an Application by running java file CreateApplicationTC, then only we can Reassign the application, "if we have to make use of existing application pass the existing application number in config file for Title parameter"
public class ReassignCaseInCheckStageTC extends DPETestBase{
	ApplicationCompletenessCheck check ;
	LoginPage login;
	
	@BeforeTest
	public void setUP(){
		
		login = new LoginPage(driver);
		check = new ApplicationCompletenessCheck(driver);
	}
	
	// Login for Application completeness check
	@Test(priority=1)
	public void login() throws InterruptedException, IOException{
		
		login.loginUsingCustomerOpsUser();
	  
	}
	@Test(priority=2)
	public void selectApplicationAndClickReassign() throws InterruptedException, IOException{
	check.clickDropDown();
	Thread.sleep(4000);
	check.selectWB();
	Thread.sleep(2000);
	check.selectApplicationFromTableOfCases();
	Thread.sleep(4000);
	
	check.clickActionsReAssign();
	Thread.sleep(4000);
	check.assignToOperator();
	Thread.sleep(4000);
	check.clickReAssignButton();
	
	Thread.sleep(4000);
	check.clickLogOff();
		
	}
	
	@Test(priority=3)
	public void loginWithReAssignedOperatorAndVerifyApplicaiton() throws InterruptedException, IOException{
		check.typeUserNameAssignedOperator();
		check.typePasswordAssignedOperator();
		check.clickSubmit();
		check.clickMyWorkFromMenu();
		Thread.sleep(4000);
		check.selectApplicationFromTableOfCasesForReassignOperator();
				
	}
	
}
