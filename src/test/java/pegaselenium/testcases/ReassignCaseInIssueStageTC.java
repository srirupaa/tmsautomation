package pegaselenium.testcases;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import pegaselenium.check.ApplicationCompletenessCheck;
import pegaselenium.issue.ReassignCaseInIssueStage;

public class ReassignCaseInIssueStageTC {
ReassignCaseInIssueStage issue ;


	
	@BeforeTest
	public void setUP(){
	
		System.setProperty("webdriver.chrome.driver","C:\\Selenium\\chromedriver.exe");
		WebDriver driver = new ChromeDriver();
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
		driver.navigate().to("https://nswpe-sit1.pegacloud.io/prweb/beEBp4uRVTogorRwSwWqbOtn9IL2fwdI*/!STANDARD");
		issue = new ReassignCaseInIssueStage(driver);
	}
	
	// Login for Application completeness check
	@Test(priority=1)
	public void login() throws InterruptedException{
		
		issue.typeUserName();
		issue.typePassword();
		issue.clickSubmit();
	    Thread.sleep(2000);
	  
	}
	
	@Test(priority=2)
	public void selectApplicationAndClickReassign() throws InterruptedException, IOException{
	
	issue.clickDropDown();
	Thread.sleep(4000);
	issue.selectWB();
	Thread.sleep(2000);
	issue.selectApplicationForReassignInIssueStage();
	Thread.sleep(4000);
	
	issue.clickActionsReAssign();
	Thread.sleep(4000);
	issue.assignToOperator();
	Thread.sleep(4000);
	issue.clickReAssignButton();
	
	Thread.sleep(4000);
	issue.clickLogOff();
	
	
	
	}
	
	@Test(priority=3)
	public void loginWithReAssignedOperatorAndVerifyApplicaiton() throws InterruptedException, IOException{
		
		issue.typeUserNameAssignedOperator();
		issue.typePasswordAssignedOperator();
		issue.clickSubmit();
		issue.clickMyWorkFromMenu();
		Thread.sleep(4000);
		issue.selectApplicationFromTableOfCasesForReassignOperator();
		
		
		
	}

}
