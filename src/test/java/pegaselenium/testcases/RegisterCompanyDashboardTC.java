package pegaselenium.testcases;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import pegaselenium.apply.RegisterCompanyDashboard;
import pegaselenium.apply.RegisterIndividualDashboard;
import pegaselenium.login.LoginPage;

public class RegisterCompanyDashboardTC {
	
RegisterCompanyDashboard regCompany;
LoginPage login;
	

	@BeforeTest
	public void setUP(){
		System.setProperty("webdriver.chrome.driver","C:\\Selenium\\chromedriver.exe");
		WebDriver driver = new ChromeDriver();
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
		driver.navigate().to("https://nswpe-sit1.pegacloud.io/prweb/beEBp4uRVTogorRwSwWqbOtn9IL2fwdI*/!STANDARD");
		login = new LoginPage(driver);
		regCompany = new RegisterCompanyDashboard(driver);
	}
	
	@Test(priority=1)
	public void registerCompanyByNameAutoStreetAddress() throws InterruptedException, IOException{
		
		login.loginUsingCustomerOpsUser();
		login.clickSubmit();
		Thread.sleep(4000);
		regCompany.clickRegisterCompanyMenu();
		regCompany.searchByName();
		regCompany.inputTASApplicantID();
		Thread.sleep(4000);
		regCompany.inputTASContactID();
		Thread.sleep(4000);
		
		
		regCompany.enterAutoStreetAddress();
		Thread.sleep(2000);
		regCompany.selectCheckBoxSameAddressForPostalAddress();
		
		regCompany.clickSubmitCompany();
		Thread.sleep(2000);
		regCompany.clickContinue();
		Thread.sleep(4000);
		
		
		
	
		
		
	}
	
	@Test(priority=2)
	public void registerCompanyByABNAutoStreetAddress() throws InterruptedException{
		

		regCompany.clickRegisterCompanyMenu();
		Thread.sleep(4000);
		regCompany.searchByName();
		Thread.sleep(4000);
		regCompany.searchByABN();
		
		regCompany.enterAutoStreetAddress();
		Thread.sleep(4000);
		regCompany.enterAutoPostalAddress();
		
		regCompany.inputTASApplicantID();
		Thread.sleep(4000);
		regCompany.inputTASContactID();
		Thread.sleep(4000);
		regCompany.clickSubmitCompany();
		Thread.sleep(2000);
		regCompany.clickContinue();
		
	}

	
	@Test(priority=3)
	public void registerCompanyByACNAutoStreetAddress() throws InterruptedException{
		
		regCompany.clickRegisterCompanyMenu();
		regCompany.searchByACN();
		
	
		regCompany.enterAutoStreetAddress();
		Thread.sleep(2000);
		regCompany.selectCheckBoxSameAddressForPostalAddress();
		Thread.sleep(2000);
		
		regCompany.inputTASApplicantID();
		Thread.sleep(4000);
		regCompany.inputTASContactID();
		Thread.sleep(4000);
		regCompany.clickSubmitCompany();
		Thread.sleep(2000);
		regCompany.clickContinue();
	}
	
	
	@Test(priority=4)
	public void registerCompanyByABNManualStreetAddress() throws InterruptedException{
		
		regCompany.clickRegisterCompanyMenu();
		Thread.sleep(4000);
		regCompany.searchByName();
		Thread.sleep(4000);
		regCompany.searchByABN();
	
		regCompany.clickNotFoundForStreetAddress();
		Thread.sleep(4000);
		regCompany.inputStreetAddressLine1();
		Thread.sleep(2000);
		regCompany.inputStreetAddressLine2();
		Thread.sleep(2000);
		regCompany.cityForStreetAddress();
		Thread.sleep(4000);
		regCompany.postalCodeForStreetAddress();
		Thread.sleep(4000);
		regCompany.stateForStreetAddress();
		Thread.sleep(4000);
		regCompany.countryForStreetAddress();
		Thread.sleep(4000);
		regCompany.selectCheckBoxSameAddressForPostalAddress();
		Thread.sleep(4000);
				
		regCompany.inputTASApplicantID();
		Thread.sleep(4000);
		regCompany.inputTASContactID();
		Thread.sleep(4000);
		regCompany.clickSubmitCompany();
		Thread.sleep(2000);
		regCompany.clickContinue();
		
	}
	
	@Test(priority=5)
	public void registerCompanyByABNManualPostalAddress() throws InterruptedException{
		
		regCompany.typeUserName();
		regCompany.typePassword();
		regCompany.clickSubmit();
		Thread.sleep(4000);
		regCompany.clickRegisterCompanyMenu();
		Thread.sleep(4000);
		regCompany.searchByName();
		Thread.sleep(4000);
		regCompany.searchByABN();

		regCompany.enterAutoStreetAddress();
		
		regCompany.clickNotFoundForPostalAddress();
		Thread.sleep(4000);
		regCompany.inputPostalAddressLine1();
		Thread.sleep(2000);
		regCompany.inputPostalAddressLine2();
		Thread.sleep(2000);
		regCompany.cityPostalAddress();
		Thread.sleep(4000);
		regCompany.postalCodeForPostalAddress();
		Thread.sleep(4000);
		regCompany.stateForPostalAddress();
		Thread.sleep(4000);
		regCompany.countryForPostalAddress();
		Thread.sleep(4000);
		
		regCompany.inputTASApplicantID();
		Thread.sleep(4000);
		regCompany.inputTASContactID();
		Thread.sleep(4000);
		regCompany.clickSubmitCompany();
		Thread.sleep(2000);
		regCompany.clickContinue();
		
	}
	
	@Test(priority=6)
public void registerCompanyByABNManualStreetAndManualPostalAddress() throws InterruptedException{
		
		regCompany.clickRegisterCompanyMenu();
		Thread.sleep(4000);
		regCompany.searchByName();
		Thread.sleep(4000);
		regCompany.searchByABN();
		
		regCompany.clickNotFoundForStreetAddress();
		Thread.sleep(4000);
		regCompany.inputStreetAddressLine1();
		Thread.sleep(2000);
		regCompany.inputStreetAddressLine2();
		Thread.sleep(2000);
		regCompany.cityForStreetAddress();
		Thread.sleep(4000);
		regCompany.postalCodeForStreetAddress();
		Thread.sleep(4000);
		regCompany.stateForStreetAddress();
		Thread.sleep(4000);
		regCompany.countryForStreetAddress();
		Thread.sleep(4000);;
		
		regCompany.clickNotFoundForPostalAddress();
		Thread.sleep(4000);
		regCompany.inputPostalAddressLine1();
		Thread.sleep(2000);
		regCompany.inputPostalAddressLine2();
		Thread.sleep(2000);
		regCompany.cityPostalAddress();
		Thread.sleep(4000);
		regCompany.postalCodeForPostalAddress();
		Thread.sleep(4000);
		regCompany.stateForPostalAddress();
		Thread.sleep(4000);
		regCompany.countryForPostalAddress();
		Thread.sleep(4000);
		
		regCompany.inputTASApplicantID();
		Thread.sleep(4000);
		regCompany.inputTASContactID();
		Thread.sleep(4000);
		regCompany.clickSubmitCompany();
		Thread.sleep(2000);
		regCompany.clickContinue();
		
	}
	
}
