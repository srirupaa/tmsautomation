/**
 * 
 */
package pegaselenium.testcases;

import java.io.IOException;
import java.io.InputStream;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import pegaselenium.apply.RegisterIndividualDashboard;

/**
 * @author bejjarv
 *
 */

public class RegisterIndividualDashboardTC {
 
	RegisterIndividualDashboard regIndividual;
	
	

	@BeforeTest
	public void setUP(){
		System.setProperty("webdriver.chrome.driver","C:\\Selenium\\chromedriver.exe");
		WebDriver driver = new ChromeDriver();
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
		driver.navigate().to("https://nswpe-sit1.pegacloud.io/prweb/beEBp4uRVTogorRwSwWqbOtn9IL2fwdI*/!STANDARD");
 regIndividual = new RegisterIndividualDashboard(driver);
	}
	
	@Test(priority=1)
	public void registerIndividualManualStreetAddressAndManualPostalAddress() throws InterruptedException{
		
	regIndividual.typeUserName();
	regIndividual.typePassword();
	regIndividual.clickSubmit();
	Thread.sleep(4000);
	regIndividual.clickRegisterIndividualMenu();
	regIndividual.fillFirstNameAndSurname();
	Thread.sleep(4000);
	regIndividual.enterMobile();
	Thread.sleep(4000);
	regIndividual.clickNotFoundForStreetAddress();
	Thread.sleep(4000);
	regIndividual.inputStreetAddressLine1();
	Thread.sleep(2000);
	regIndividual.inputStreetAddressLine2();
	Thread.sleep(2000);
	regIndividual.cityForStreetAddress();
	Thread.sleep(4000);
	regIndividual.postalCodeForStreetAddress();
	Thread.sleep(4000);
	regIndividual.stateForStreetAddress();
	Thread.sleep(4000);
	regIndividual.countryForStreetAddress();
	Thread.sleep(4000);

	
	regIndividual.clickNotFoundForPostalAddress();
	Thread.sleep(4000);
	regIndividual.inputPostalAddressLine1();
	Thread.sleep(2000);
	regIndividual.inputPostalAddressLine2();
	Thread.sleep(2000);
	regIndividual.cityPostalAddress();
	Thread.sleep(4000);
	regIndividual.postalCodeForPostalAddress();
	Thread.sleep(4000);
	regIndividual.stateForPostalAddress();
	Thread.sleep(4000);
	regIndividual.countryForPostalAddress();
	Thread.sleep(4000);


		regIndividual.clickSubmitRegisterIndividual();
		Thread.sleep(4000);
		regIndividual.verifyAcknowledge();
		regIndividual.clickContinue();
		Thread.sleep(4000);
		
	}
	


	
	// Street Address Auto populate
	@Test(priority=2)
	public void registerIndividualAutoPopulateStreetAddress() throws InterruptedException{

		regIndividual.clickRegisterIndividualMenu();
		regIndividual.fillFirstNameAndSurname();
		Thread.sleep(4000);
		regIndividual.enterMobile();
		regIndividual.enterAutoStreetAddress();
		regIndividual.selectCheckBoxSameAddressForPostalAddress();
		regIndividual.clickSubmitRegisterIndividual();
		Thread.sleep(4000);
		regIndividual.verifyAcknowledge();
		regIndividual.clickContinue();
		Thread.sleep(4000);
		
	}
	
	
	// Postal Address Auto populate
	
	@Test(priority=3)
	public void registerIndividualAutoPopulatePostalAddress() throws InterruptedException{

		regIndividual.clickRegisterIndividualMenu();
		regIndividual.fillFirstNameAndSurname();
		Thread.sleep(4000);
		regIndividual.enterMobile();
		regIndividual.enterAutoStreetAddress();
		
		Thread.sleep(4000);
		
		regIndividual.enterAutoPostalAddress();
		Thread.sleep(4000);
		regIndividual.clickSubmitRegisterIndividual();
		Thread.sleep(4000);
		regIndividual.verifyAcknowledge();
		regIndividual.clickContinue();
		
		
	}
	
	// Manual Street Address and copy to postal address
	
	@Test(priority=4)
	public void registerIndividualManualStreetAddressAndCopy() throws InterruptedException{
		
		regIndividual.clickRegisterIndividualMenu();
		regIndividual.fillFirstNameAndSurname();
		Thread.sleep(4000);
		regIndividual.enterMobile();
		Thread.sleep(4000);
		regIndividual.clickNotFoundForStreetAddress();
		Thread.sleep(4000);
		regIndividual.inputStreetAddressLine1();
		Thread.sleep(2000);
		regIndividual.inputStreetAddressLine2();
		Thread.sleep(2000);
		regIndividual.cityForStreetAddress();
		Thread.sleep(4000);
		regIndividual.postalCodeForStreetAddress();
		Thread.sleep(4000);
		regIndividual.stateForStreetAddress();
		Thread.sleep(4000);
		regIndividual.countryForStreetAddress();
		Thread.sleep(4000);
		regIndividual.selectCheckBoxSameAddressForPostalAddress();
		Thread.sleep(4000);
		regIndividual.clickSubmitRegisterIndividual();
		Thread.sleep(4000);
		regIndividual.verifyAcknowledge();
		regIndividual.clickContinue();
		
	}
	
	// Manual postal Address and auto street address
	
	@Test(priority=5)
	public void registerIndividualManualPostalAddress() throws InterruptedException{

		regIndividual.clickRegisterIndividualMenu();
		regIndividual.fillFirstNameAndSurname();
		Thread.sleep(4000);
	regIndividual.enterMobile();
		regIndividual.enterAutoStreetAddress();
	
	regIndividual.clickNotFoundForPostalAddress();
	Thread.sleep(4000);
	regIndividual.inputPostalAddressLine1();
	Thread.sleep(2000);
	regIndividual.inputPostalAddressLine2();
	Thread.sleep(2000);
	regIndividual.cityPostalAddress();
	Thread.sleep(4000);
	regIndividual.postalCodeForPostalAddress();
	Thread.sleep(4000);
	regIndividual.stateForPostalAddress();
	Thread.sleep(4000);
	regIndividual.countryForPostalAddress();
	Thread.sleep(4000);


		regIndividual.clickSubmitRegisterIndividual();
		Thread.sleep(4000);
		regIndividual.verifyAcknowledge();
		regIndividual.clickContinue();
		
	}
	
		
		}




