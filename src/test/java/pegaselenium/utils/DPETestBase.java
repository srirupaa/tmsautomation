package pegaselenium.utils;

import java.io.IOException;
import java.text.ParseException;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.safari.SafariDriver;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.Optional;
import org.testng.annotations.Parameters;


public class DPETestBase {
	
	  protected WebDriver driver ;
		protected static Environment env;

	
	@BeforeSuite
	@Parameters({"Env", "Browser"})
	public void setUp(@Optional("SIT")  String Environment , @Optional("Chrome") String Browser) throws IOException, ParseException {
		
		driver = initiateBrowser(Browser);
		getEnvURL(Environment);
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
		driver.get(env.getBaseURL());
	}
	
	
	

	@AfterSuite(alwaysRun=true)
	public void tearDown() throws Exception{
		try{
			driver.close();
			driver.quit();
		}catch(WebDriverException e){
			e.printStackTrace();
		}
	}
	
	public WebDriver initiateBrowser(String Browser){
		if (Browser.equalsIgnoreCase("firefox"))
		{
			driver = new FirefoxDriver();
		}
		
		else if(Browser.equalsIgnoreCase("ie")){
			//DesiredCapabilities capabilities = DesiredCapabilities.internetExplorer();
			//capabilities.setCapability(InternetExplorerDriver.INTRODUCE_FLAKINESS_BY_IGNORING_SECURITY_DOMAINS, true);
			System.setProperty("webdriver.ie.driver" , "drivers\\IEDriverServer_x64_2.45.0\\IEDriverServer.exe");
			driver =  new InternetExplorerDriver();
		}
		
		else if(Browser.equalsIgnoreCase("Chrome")){
			
			System.setProperty("webdriver.chrome.driver" , "drivers\\chromedriver\\chromedriver.exe");
			driver = new ChromeDriver();
		}
		
		else if (Browser.equalsIgnoreCase("Safari")){
			
			driver = new SafariDriver();
			
		}
		
		else{
			
			System.setProperty("webdriver.chrome.driver" , "drivers\\chromedriver\\chromedriver.exe");
			driver = new ChromeDriver();
		}
		return driver;
	}
	
	public void getEnvURL(String environ){
		
		
		if(environ.equalsIgnoreCase("SIT")){
			 env = new Environment(Environment.Env.SIT);
		}
		
		else if(environ.equalsIgnoreCase("UAT")){
			 env = new Environment(Environment.Env.UAT);
		}
		
		else if(environ.equalsIgnoreCase("DEV")){
			 env = new Environment(Environment.Env.DEV);
		}
		
		else if(environ.equalsIgnoreCase("DEVCLONE")){
			 env = new Environment(Environment.Env.DEVCLONE);
		}

		else{
			
			 env = new Environment(Environment.Env.SIT);
		}
		
		
	}

}
