package pegaselenium.utils;


import java.io.IOException;
import java.util.concurrent.TimeUnit;
import java.util.function.Function;

import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.FluentWait;
import org.openqa.selenium.support.ui.Wait;



public class DriverPage {
	
	protected static WebDriver driver;

	public DriverPage(WebDriver driver) {
		// TODO Auto-generated constructor stub
		this.driver = driver;
	}
	
	public void pressContinue(){
		driver.findElement(By.name("pyCaseActionAreaButtons_pyWorkPage_18")).click();
	}
	
	public void pressSave(){
		driver.findElement(By.name("pyCaseActionAreaButtons_pyWorkPage_17")).click();
	}
	
	public void pressBack() {
		driver.findElement(By.name("pyCaseActionAreaButtons_pyWorkPage_15")).click();

	}
	
	public void pressFinish() throws IOException {
		driver.findElement(By.name("pyCaseActionAreaButtons_pyWorkPage_19")).click();
	}
	
	public WebElement waitUntilElementPresent(final By locator){
		WebElement clickseleniumlink = null;
		Wait<WebDriver> wait = new FluentWait<WebDriver>(driver)							
				.withTimeout(30, TimeUnit.SECONDS) 			
				.pollingEvery(5, TimeUnit.SECONDS) 			
				.ignoring(NoSuchElementException.class);
		
		try{
			
			 clickseleniumlink = wait.until(new Function<WebDriver, WebElement>(){
				public WebElement apply(WebDriver driver ) {
					return driver.findElement(locator);
				}
			
			});
		
		}
		catch(Exception e){
			e.printStackTrace();
		}
		return clickseleniumlink;
	}
	


	public static WebDriver getDriverInstance() {
		// TODO Auto-generated method stub
		return driver;
	}
	
		public void navigateToDashboard() throws InterruptedException{
				
			openHamBurgerMenu();
			Thread.sleep(3000);
			WebElement elem= waitUntilElementPresent(By.xpath("//span[text()='Dashboard']"));
			elem.click();
		}
		
		public void openHamBurgerMenu(){
			driver.findElement(By.xpath("//a[@id='appview-nav-toggle-one']")).click();
		}
	
		public void pressActionsBtn(){
			driver.findElement(By.name("pyCaseHeaderOuter_pyWorkPage_16")).click();
		}
		

		public void selectProvideAdvice(){
			driver.findElement(By.xpath("//span[text()='Provide Advice']")).click();
		}
}
