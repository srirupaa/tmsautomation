/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pegaselenium.utils;

import java.io.File;
import java.io.IOException;
import java.util.Random;
import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;

/**
 *
 * @author Srirupa
 */
public class Environment {

    private final String SITURL = "https://nswpe-sit1.pegacloud.io/prweb/beEBp4uRVTogorRwSwWqbOtn9IL2fwdI*/!STANDARD";
    private final String DEVURL = "https://nswpe-dev1.pegacloud.io/prweb/beEBp4uRVTogorRwSwWqbOtn9IL2fwdI*/!STANDARD";
    private final String DEVCLONEURL = "https://nswpe-dev1.pegacloud.io/prweb/beEBp4uRVTogorRwSwWqbOtn9IL2fwdI*/!STANDARD";
    private final String UATURL = "https://nswdrg-uat1.pegacloud.net/prweb/ElPGmjCjpVdakYUxVnJH4G2dtBmEhp1v*/!STANDARD?&";
   
    
    public enum Env {
    	SIT
        ,DEV
        ,DEVCLONE
        ,UAT

    }
    
    
    private final Env currentEnvironment;
    
    public Environment(Environment.Env env) {
        this.currentEnvironment = env;
    }
    
    public String getBaseURL() {
        switch(this.currentEnvironment) {
            case SIT:
                return SITURL;

            case DEV:
                return DEVURL;                
                
            case DEVCLONE:
                return DEVCLONEURL;
            case UAT:
                return UATURL;  
                         
            default:
                return SITURL;
        }
    }
    
   
   
}
