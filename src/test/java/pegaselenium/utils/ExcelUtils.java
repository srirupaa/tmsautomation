package pegaselenium.utils;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;

import org.apache.commons.compress.archivers.dump.InvalidFormatException;
import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public class ExcelUtils {

	File des;
	XSSFSheet sheet;
	XSSFWorkbook wb;

	public ExcelUtils(String location) {
		try {
			des = new File(location);
			FileInputStream fis = new FileInputStream(des);
			wb = new XSSFWorkbook(fis);

		} catch (Exception e) {
			System.out.println(e.getMessage());
		}

	}

	public void writeDataToExcel(String sheetName , String str) throws Exception {
		sheet = wb.getSheet(sheetName);
		int rows = sheet.getLastRowNum();
		System.out.println("Total number of Rows to write : " + (rows + 1));
		for(int j=0;j<100;j++){
			
			try{
			if(sheet.getRow(1).getCell(j).getStringCellValue().isEmpty()){
				sheet.getRow(1).createCell(j).setCellValue(str);
				break;
			}
			}
			catch(NullPointerException e)
			{
				
			}
		
			
		}
		
		FileOutputStream fos = new FileOutputStream(des);
		wb.write(fos);
		wb.close();
	}

	public String[] getXLDataLatestByRow(String sheetname, int rowNo) throws IOException {

		 sheet = wb.getSheet(sheetname);

		int rowNum = sheet.getLastRowNum() + 1;
		System.out.println("Number of rows:" + rowNum);
		int colNum = sheet.getRow(0).getLastCellNum();
		System.out.println("Number of columns:" + colNum);
		String[] data = new String[colNum];
		// This code is for complete excel reading.
		/*
		 * for (int i=0; i<rowNum; i++){ XSSFRow row = ws.getRow(i); for (int
		 * j=0; j<colNum; j++){ XSSFCell cell = row.getCell(j); String value =
		 * cellToString(cell); data[i][j] = value;
		 * System.out.println("The value is" + value);
		 * 
		 * } }
		 */
		XSSFRow row = sheet.getRow(rowNo);
		for (int j = 0; j < colNum; j++) {
			XSSFCell cell = row.getCell(j);

			String value = "";
			// System.out.println(cell);

			try {
				cell.setCellType(CellType.STRING);
				value = cell.getStringCellValue();
			} catch (NullPointerException e) {

			}

			data[j] = value;

			// System.out.println("The value is" + value);
			// System.out.println("The data value is" + data[j]);
		}
		return data;
	}

	public String[][] getXLDataByPOI(String sheetname) throws IOException, InvalidFormatException {

		int index = 0;

		sheet = wb.getSheet(sheetname);
		int rows = sheet.getPhysicalNumberOfRows();
		int columns = sheet.getRow(1).getPhysicalNumberOfCells();

		System.out.println("Total rows:" + rows);
		System.out.println("Total columns:" + columns);
		String a[][] = new String[rows - 1][columns];
		try {

			for (int i = 1; i < rows; i++)

			{
				// System.out.println("No of index:"+ index);
				for (int j = 0; j < columns; j++) {

					// System.out.println("No of row:"+ i);
					// System.out.println("No of Column:"+ j);
					// System.out.println("No of index:"+ index);
					a[index][j] = sheet.getRow(i).getCell(j).toString();
					// System.out.println(j+" and "+index+" "+a[index][j]);
				}
				index = index + 1;
				// System.out.println("No of index:"+ index);
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
		return a;
	}

}
