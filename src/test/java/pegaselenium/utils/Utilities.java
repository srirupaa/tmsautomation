package pegaselenium.utils;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Random;

import org.apache.commons.compress.archivers.dump.InvalidFormatException;
import org.apache.commons.io.FileUtils;
import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;

public class Utilities {

	public final String randomString(final int length) {
		char[] chars = "abcdefghijklmnopqrstuvwxyz".toCharArray();
		Random r = new Random();
		StringBuilder sb = new StringBuilder();
		for (int i = 0; i < length; ++i) {
			char c = chars[r.nextInt(chars.length)];
			sb.append(c);
		}
		return sb.toString();
	}

	public static void captureSnapshot(WebDriver driver) {
		try {
			File scrFile = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
			FileUtils.copyFile(scrFile, new File("C:\\reports\\" + System.currentTimeMillis() + ".png"));
		} catch (IOException e) {
			System.out.println(e.getStackTrace());
		}
	}

	public static String getCurrentDate(){
		SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy");
		return  format.format(new Date());

	}
	
	

}
